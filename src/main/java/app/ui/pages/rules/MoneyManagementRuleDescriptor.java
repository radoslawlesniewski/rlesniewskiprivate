package app.ui.pages.rules;

public class MoneyManagementRuleDescriptor {
	
	public String name;
	public String type;
	public String longPosOption;
	public String shortPosOption;
	public String longPosAmount;
	public String shortPosAmount;
	public String drawdownPercent;
	public String drawdownEquity;
	public String closePos;
	public Boolean positionCB;
	public Boolean pendingOrders;
	public Boolean deactivateTradingStrategies;
	public String IPSlossAmount;
	public String IPSlossPIPs;
	public String IPTprofitAmount;
	public String IPTprofitPIPs;
	public String marginPercent;
	public Boolean openPosSameDir;
	public Boolean drawdownCB;
	public Boolean equityDrawDownCB;
	public Boolean amountCBloss;
	public Boolean pipsCBloss;
	public Boolean amountCBprofit;
	public Boolean pipsCBprofit;
	
}
