package app.ui.pages.indicators;

public class IndicatorsDescriptor {

	public String typeOfIndicator;
	public String symbol;
	public String timeFrame;
	public String jawsPeriod;
	public String name;
	public String teethPeriod;
	public String lipsPeriod;
	public String jawsShift;
	public String teethShift;
	public String lipsShift;
	public String applyTo;
	public String maMethod;
	public String period;
	public String shift;
	public String deviations;
	public String deviation;
	public String tenkanSen;
	public String kijunSen;
	public String senkouSpanB;
	public String fastEMA;
	public String slowEMA;
	public String macdSMA;
	public String step;
	public String maximum;
	public String dPeriod;
	public String kPeriod;
	public String slowing;
	public String priceField;
}
