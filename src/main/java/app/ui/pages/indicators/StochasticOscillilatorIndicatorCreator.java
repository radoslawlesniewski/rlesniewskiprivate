package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

public class StochasticOscillilatorIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[1]//div[3]/select")
	WebElement maMethod;
	
	@FindBy(xpath = "//tr[5]//div[3]/input")
	WebElement kPeriod;
	
	@FindBy(xpath = "//tr[4]//tr[4]//div[3]/input")
	WebElement dPeriod;
	
	@FindBy(xpath = "//tr[4]//tr[3]//div[3]/input")
	WebElement slowing;
	
	@FindBy(xpath = "//tr[2]//div[3]/input")
	WebElement priceField;
			
	public StochasticOscillilatorIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	
	private StochasticOscillilatorIndicatorCreator(Builder builder) {
		super(builder);
		maMethod.click();

		String xpath = "//tr[1]//div[3]/select/*[.='"+ builder.indicatorDescriptor.maMethod +"']";
		WebElement typeOneFromTheList = getDriver().findElement(By.xpath(xpath));

		Actions action = new Actions(getDriver());
		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
		
		
		kPeriod.clear();
		kPeriod.sendKeys(builder.indicatorDescriptor.kPeriod);
		dPeriod.clear();
		dPeriod.sendKeys(builder.indicatorDescriptor.dPeriod);
		slowing.clear();
		slowing.sendKeys(builder.indicatorDescriptor.slowing);
		priceField.clear();
		priceField.sendKeys(builder.indicatorDescriptor.priceField);
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		public Builder maMethod(String value) {
			indicatorDescriptor.maMethod = value;
			return this;
		}
		
		public Builder kPeriod(String value) {
			indicatorDescriptor.kPeriod = value;
			return this;
		}
		
		public Builder dPeriod(String value) {
			indicatorDescriptor.dPeriod = value;
			return this;
		}
		
		public Builder slowing(String value) {
			indicatorDescriptor.slowing = value;
			return this;
		}
		
		public Builder priceField(String value) {
			indicatorDescriptor.priceField = value;
			return this;
		}

		
		public StochasticOscillilatorIndicatorCreator build(){
			return new StochasticOscillilatorIndicatorCreator(this);
		}
	}

}