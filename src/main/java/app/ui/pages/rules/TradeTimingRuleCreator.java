package app.ui.pages.rules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class TradeTimingRuleCreator extends BasePage {

	@FindBy(xpath = "//div[1]//div[2]/div/div/div[2]/ul/li[1]/a")
	WebElement addTradeTimingRuleButton;

	@FindBy(xpath = "//div[3]//div[2]/input")
	WebElement nameOfRule;

	@FindBy(css = ".btn.btn-primary.save-btn")
	WebElement saveTradeTimingRuleButton;

	@FindBy(css = ".ui-pnotify-text")
	WebElement confirmMessage;

	public TradeTimingRuleCreator(DriverProvider provider) {
		super(provider);
	}

	protected TradeTimingRuleCreator(Builder builder) {
		super(builder.provider);
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);

		wait.until(ExpectedConditions
				.elementToBeClickable(addTradeTimingRuleButton));
		addTradeTimingRuleButton.click();

		nameOfRule.sendKeys(builder.trade.name);

		switch (builder.trade.type) {
		case "Trade Signal":
			TradeSignal tradeSignal = new TradeSignal.Builder(getProvider())
					.bearBullSignal(builder.trade.bullBearSignal).ruleType(builder.trade.ruleType)
					.indicatorField(builder.trade.indicatorField).customRule(builder.trade.customRule)
					.amount(builder.trade.amount).nameOfIndicator(builder.trade.nameOfIndicator)
					.logicOperator(builder.trade.logicOperator).symbol(builder.trade.symbol)
					.symbolOption(builder.trade.symbolOption).build();
//			tradeSignal = new TradeSignal(getProvider());
			break;
		case "Multiple Trade handling":
			MultipleTradeHandling multipleTradeHandling = new MultipleTradeHandling.Builder(
					getProvider()).sameDirection(builder.trade.sameDirectionSignal)
					.oppositeDirection(builder.trade.oppositeDirectionSignal)
					.build();
//			multipleTradeHandling = new MultipleTradeHandling(getProvider());
			break;
		}

	}

	public void saveRule() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);

		saveTradeTimingRuleButton.click();
		wait.until(ExpectedConditions.visibilityOf(confirmMessage));

	}

	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public RuleDescriptor trade = new RuleDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T name(String value) {
			trade.name = value;
			return (T) this;
		}

		public T type(String value) {
			trade.type = value;
			return (T) this;
		}

		public T sameDirection(String value) {
			trade.sameDirectionSignal = value;
			return (T) this;
		}

		public T oppositeDirection(String value) {
			trade.oppositeDirectionSignal = value;
			return (T) this;
		}

		public T bearBullSignal(String value) {
			trade.bullBearSignal = value;
			return (T) this;
		}

		public T ruleType(String value) {
			trade.ruleType = value;
			return (T) this;
		}
		
		public T nameOfIndicator(String value) {
			trade.nameOfIndicator = value;
			return (T) this;
		}

		public T indicatorField(String value) {
			trade.indicatorField = value;
			return (T) this;
		}

		public T logicOperator(String value) {
			trade.logicOperator = value;
			return (T) this;
		}
		
		public T amount(String value) {
			trade.amount = value;
			return (T) this;
		}
		
		public T symbol(String value) {
			trade.symbol = value;
			return (T) this;
		}

		public T symbolOption(String value) {
			trade.symbolOption = value;
			return (T) this;
		}
		
		public T customRule(String value) {
			trade.customRule = value;
			return (T) this;
		}

		public TradeTimingRuleCreator build() {
			return new TradeTimingRuleCreator(this);
		}

	}

}