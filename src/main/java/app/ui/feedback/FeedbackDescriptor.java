package app.ui.feedback;

public class FeedbackDescriptor {
	public String question;
	public String details;
	public String name;
	public String emailAddress;
}
