package app.ui.pages.rules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import app.ui.common.BasePage;
import app.ui.steps.AppUiSteps;
import app.ui.webdriver.DriverProvider;

public class Symbol extends BasePage {
	
	@FindBy(css = "#symbolBtn")
	WebElement symbolButton;
	
	@FindBy(css = "#amountBtn")
	WebElement amountButton;

	@FindBy(css = "#logicBtn")
	WebElement logicButton;
	
	@FindBy(xpath = "//span[.='..Select Symbol..']/..")
	WebElement symbolsList;
		
	@FindBy(xpath = "//td[3]/div/div/input")
	WebElement amountInput;
	
	@FindBy(xpath = "//span[.='..Choose Field..']/..")
	WebElement fieldsList;
	
	@FindBy(xpath = "//span[.='..Chose Operator..']/..")
	WebElement operatorsList;
	
	public Symbol(DriverProvider provider) {
		super(provider);
	}
	
	protected Symbol(Builder builder) {
		super(builder.provider);
		
		String xpath = null;
		Actions action = new Actions(getDriver());
		symbolButton.click();

		symbolsList.click();
		
		xpath = ".//*[@class='GOL5FQVBEI'][.='" + builder.symbol.symbol
				+ "']";
		WebElement symbolFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(symbolFromTheList).click().build().perform();

		fieldsList.click();

		xpath = ".//*[@class='GOL5FQVBEI'][.='"
				+ builder.symbol.symbolOption + "']";

		WebElement optionFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(optionFromTheList).click().build().perform();
		
		logicButton.click();
		
		logicButton.click();		
		operatorsList.click();
		
		xpath = ".//*[@class='GOL5FQVBEI'][.='"
				+ builder.symbol.logicOperator + "']";
		WebElement operatorFromTheList = getDriver().findElement(By.xpath(xpath));

		action.moveToElement(operatorFromTheList).click().build().perform();
		
		amountButton.click();
		amountInput.sendKeys(builder.symbol.amount);
		


		
	}
	
	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public RuleDescriptor symbol = new RuleDescriptor();


		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T symbol(String value) {
			symbol.symbol = value;
			return (T) this;
		}

		public T symbolOption(String value) {
			symbol.symbolOption = value;
			return (T) this;
		}

		public T logicOperator(String value) {
			symbol.logicOperator = value;
			return (T) this;
		}
		
		public T amount(String value) {
			symbol.amount = value;
			return (T) this;
		}
		
		public Symbol build() {
			return new Symbol(this);
		}
	}

}
