package app.ui.pages.indicators;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.StorageHolder.Type;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class BaseIndicatorsCreator extends BasePage {

	@FindBy(css = "[data-field='addIndicator']")
	WebElement addIndicatorButton;

	@FindBy(xpath = "//*[@id='selectError3_chzn']")
	WebElement indicatorsList;

	@FindBy(css = ".gwt-TextBox")
	WebElement indicatorName;

//	@FindBy(xpath = "//td/select/option")
//	List <WebElement> symbolsList;
	
//	@FindBy(css = ".gwt-Button[type='button']")
//	List <WebElement> timeFrameList;

	@FindBy(css = "[data-field='addNewIndicatorDialogBody'] #modalSave")
	WebElement saveIndicatorButton;

	@FindBy(css = ".halflings-icon.remove")
	WebElement deleteIndictaorButton;

	@FindBy(css = ".ui-pnotify-text")
	WebElement confirmMessage;
	
	@FindBy(css = ".closeLink")
	WebElement closeButton;
	

	public BaseIndicatorsCreator(DriverProvider provider) {
		super(provider);
	}
	
	public List<String> getListOfStrings(List<WebElement> List) {
		
		List<String> stringsList;
		stringsList = new ArrayList<String>();
		
		for (WebElement element: List) {
			
			stringsList.add(element.getText());
		}
		return stringsList;
	}
	
	
	public void addEveryIndicatorOfType(String typeOfIndicator){
		addIndicatorButton.click();
		indicatorsList.click();
		String xpath = ".//*[@class='GOL5FQVBEI'][.='" + typeOfIndicator + "']";
		WebElement typeFromTheList = getDriver().findElement(By.xpath(xpath));
		
		Actions action = new Actions(getDriver());
		action
				.moveToElement(typeFromTheList)
				.click()
				.build().perform();
	
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		
		//For symbols
				
		xpath = "//*[@data-field='symbol']/option";
		List <WebElement> symbolsList = getDriver().findElements(By.xpath(xpath));
			
		// For timeFrames
		
		String css = ".gwt-Button[type='button']";
		List <WebElement> timeFramesList = getDriver().findElements(By.cssSelector(css));
		int a = 1,b = 2;
		
		for(String symbol: getListOfStrings(symbolsList)) {
			
			css = "[data-field='symbol'] [value='" + symbol + "']";
			WebElement symbolW = getDriver().findElement(By.cssSelector(css));
					
			action
					.moveToElement(symbolW)
					.clickAndHold(symbolW)
					.release().build().perform();
			
			logger.info("Symbol1" + symbolW); ////////!!!!!!!!!!!!!!!!!!!!!!
			
			for(String timeFrame: getListOfStrings(timeFramesList)) {
				
				xpath = "//*[@type='button'] [.='" + timeFrame + "']";
				WebElement timeFrameW = getDriver().findElement(By.xpath(xpath));
				timeFrameW.click();
				logger.info("TimeFrame"+ a + getDriver().findElement(By.xpath(xpath)));
				a++;
				
				String name = typeOfIndicator + symbolW.getText() + timeFrameW.getText();
				indicatorName.sendKeys(name);
				saveIndicatorButton.click();
				
				String text = "Indicator is saved with";
				wait.until(ExpectedConditions.visibilityOf(confirmMessage));
				wait.until(ExpectedConditions.textToBePresentInElement(confirmMessage,
						text));
				
				css = ".icon-remove";
				action
						.moveToElement(confirmMessage)
						.click()
						.build().perform();
				getDriver().findElement(By.cssSelector(css)).click();
						
				addIndicatorButton.click();
				
				indicatorsList.click();
				
				xpath = ".//*[@class='GOL5FQVBEI'][.='" + typeOfIndicator + "']";
				typeFromTheList = getDriver().findElement(By.xpath(xpath));
				action
						.moveToElement(typeFromTheList)
						.click()
						.build().perform();
				
				css = "[data-field='symbol'] [value='" + symbol + "']";
				symbolW = getDriver().findElement(By.cssSelector(css));
				logger.info("Symbol"+ b + getDriver().findElement(By.cssSelector(css)));
				b++;
				action
						.moveToElement(symbolW)
						.clickAndHold(symbolW)
						.release().build().perform();
			}

		}
		closeButton.click();
	}

}
