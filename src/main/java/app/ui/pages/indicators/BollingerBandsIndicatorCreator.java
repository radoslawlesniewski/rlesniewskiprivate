package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

public class BollingerBandsIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[1]//div[3]/select")
	WebElement applyTo;
	
	@FindBy(xpath = "//tr[2]//div[3]/input")
	WebElement period;
	
	@FindBy(xpath = "//tr[3]//div[3]/input")
	WebElement shift;
	
	@FindBy(xpath = "//tr[4]//tr[4]//div[3]/input")
	WebElement deviations;
	
			
	public BollingerBandsIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	
	private BollingerBandsIndicatorCreator(Builder builder) {
		super(builder);
		
		if(!builder.indicatorDescriptor.applyTo.isEmpty()){
		applyTo.click();

		String xpath = ".//tr[1]//div[3]/select/*[.='"+ builder.indicatorDescriptor.applyTo +"']";
		WebElement typeFromTheList = getDriver().findElement(By.xpath(xpath));

		Actions action = new Actions(getDriver());
		action.moveToElement(typeFromTheList).clickAndHold(typeFromTheList)
		.release().build().perform();}
		if(!builder.indicatorDescriptor.period.isEmpty()){
		period.clear();
		period.sendKeys(builder.indicatorDescriptor.period);
		}
		if(!builder.indicatorDescriptor.shift.isEmpty()){
		shift.clear();
		shift.sendKeys(builder.indicatorDescriptor.shift);}
		if(!builder.indicatorDescriptor.deviations.isEmpty()){
		deviations.clear();
		deviations.sendKeys(builder.indicatorDescriptor.deviations);}
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		public Builder applyTo(String value) {
			indicatorDescriptor.applyTo = value;
			return this;
		}
		
		public Builder period(String value) {
			indicatorDescriptor.period = value;
			return this;
		}
		
		public Builder shift(String value) {
			indicatorDescriptor.shift = value;
			return this;
		}
		
		public Builder deviations(String value) {
			indicatorDescriptor.deviations = value;
			return this;
		}
		
		
		public BollingerBandsIndicatorCreator build(){
			return new BollingerBandsIndicatorCreator(this);
		}
	}

}