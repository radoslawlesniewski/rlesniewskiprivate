package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import app.ui.webdriver.DriverProvider;

public class AlligatorIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[1]//div[3]/input")
	WebElement jawsPeriod;
	
	@FindBy(xpath = "//tr[2]//div[3]/input")
	WebElement teethPeriod;
	
	@FindBy(xpath = "//tr[3]//div[3]/input")
	WebElement lipsPeriod;
	
	@FindBy(xpath = "//tr[4]//tr[4]//div[3]/input")
	WebElement jawsShift;
	
	@FindBy(xpath = "//tr[5]//div[3]/input")
	WebElement teethShift;
	
	@FindBy(xpath = "//tr[6]//div[3]/input")
	WebElement lipsShift;
	
	@FindBy(xpath = "//tr[7]//div[3]/select")
	WebElement applyTo;
	
	@FindBy(xpath = "//tr[8]//div[3]/select")
	WebElement maMethod;
	
	public AlligatorIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	
	private AlligatorIndicatorCreator(Builder builder) {
		super(builder);
		jawsPeriod.clear();
		jawsPeriod.sendKeys(builder.indicatorDescriptor.jawsPeriod);
		teethPeriod.clear();
		teethPeriod.sendKeys(builder.indicatorDescriptor.teethPeriod);
		lipsPeriod.clear();
		lipsPeriod.sendKeys(builder.indicatorDescriptor.lipsPeriod);
		jawsShift.clear();
		jawsShift.sendKeys(builder.indicatorDescriptor.jawsShift);
		teethShift.clear();
		teethShift.sendKeys(builder.indicatorDescriptor.teethShift);
		lipsShift.clear();
		lipsShift.sendKeys(builder.indicatorDescriptor.lipsShift);
		
		applyTo.click();

		String xpath = "//tr[7]//div[3]/select/*[.='"+ builder.indicatorDescriptor.applyTo +"']";
		WebElement typeOneFromTheList = getDriver().findElement(By.xpath(xpath));

		Actions action = new Actions(getDriver());
		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
		
		maMethod.click();

		xpath = "//tr[8]//div[3]/select/*[.='"+ builder.indicatorDescriptor.maMethod +"']";
		typeOneFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
	
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		public Builder jawsPeriod(String value) {
			indicatorDescriptor.jawsPeriod = value;
			return this;
		}
		
		public Builder teethPeriod(String value) {
			indicatorDescriptor.teethPeriod = value;
			return this;
		}
		
		public Builder lipsPeriod(String value) {
			indicatorDescriptor.lipsPeriod = value;
			return this;
		}
		
		public Builder jawsShift(String value) {
			indicatorDescriptor.jawsShift = value;
			return this;
		}
		
		public Builder teethShift(String value) {
			indicatorDescriptor.teethShift = value;
			return this;
		}
		
		public Builder lipsShift(String value) {
			indicatorDescriptor.lipsShift = value;
			return this;
		}
		
		public Builder applyTo(String value) {
			indicatorDescriptor.applyTo = value;
			return this;
		}
		
		public Builder maMethod(String value) {
			indicatorDescriptor.maMethod = value;
			return this;
		}
		
		public AlligatorIndicatorCreator build(){
			return new AlligatorIndicatorCreator(this);
		}
	}
	
	
}
