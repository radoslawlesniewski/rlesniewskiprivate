package app.ui.pages.rules;

import org.openqa.selenium.WebDriver;

import app.ui.common.BasePage;
import app.ui.pages.signals.SignalCreator;
import app.ui.pages.rules.TradeTimingRuleCreator;
import app.ui.webdriver.DriverProvider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class TradeSignal extends BasePage {

	@FindBy(xpath = "//div[3]//div[1]/div[2]/div//div[2]/div/button[1]")
	WebElement tradeSignalButton;

	@FindBy(xpath = ".//*[@id='bullBearPanel']/div/div[2]/div/button[1]")
	WebElement bullSignalButton;
	
	@FindBy(xpath = ".//*[@id='bullBearPanel']/div/div[2]/div/button[2]")
	WebElement bearSignalButton;
	
	@FindBy(xpath = "//*[@id='ruleTypePanel']/div/div[2]/div/button[1]")
	WebElement instantMarketOrderButton;
	
	@FindBy(xpath = "//*[@id='ruleTypePanel']/div/div[2]/div/button[2]")
	WebElement customRuleButton;
	
	public TradeSignal(DriverProvider provider) {
		super(provider);
	}

	protected TradeSignal(Builder builder) {
		super(builder.provider);

		tradeSignalButton.click();

		switch (builder.trade.bullBearSignal) {
		case "Bull Signal":
			bullSignalButton.click();
			break;
		case "Bear Signal":
			bearSignalButton.click();
			break;
		}

		switch (builder.trade.ruleType) {
		case "Instant market order":
			instantMarketOrderButton.click();
			break;
		case "Custom Rule":
			customRuleButton.click();

			switch (builder.trade.customRule) {
			case "Indicator":
				Indicator indicator = new Indicator.Builder(getProvider())
						.nameOfIndicator(builder.trade.nameOfIndicator)
						.indicatorField(builder.trade.indicatorField)
						.logicOperator(builder.trade.logicOperator)
						.amount(builder.trade.amount).build();
				indicator = new Indicator(getProvider());
				break;
			case "Symbol":
				Symbol symbol = new Symbol.Builder(getProvider())
						.symbol(builder.trade.symbol)
						.symbolOption(builder.trade.symbolOption)
						.logicOperator(builder.trade.logicOperator)
						.amount(builder.trade.amount).build();
				symbol = new Symbol(getProvider());
				break;
			}
			break;
		}

	}

	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public RuleDescriptor trade = new RuleDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T bearBullSignal(String value) {
			trade.bullBearSignal = value;
			return (T) this;
		}

		public T ruleType(String value) {
			trade.ruleType = value;
			return (T) this;
		}
		
		public T nameOfIndicator(String value) {
			trade.nameOfIndicator = value;
			return (T) this;
		}

		public T indicatorField(String value) {
			trade.indicatorField = value;
			return (T) this;
		}

		public T logicOperator(String value) {
			trade.logicOperator = value;
			return (T) this;
		}
		
		public T amount(String value) {
			trade.amount = value;
			return (T) this;
		}
		
		public T symbol(String value) {
			trade.symbol = value;
			return (T) this;
		}

		public T symbolOption(String value) {
			trade.symbolOption = value;
			return (T) this;
		}
		
		public T customRule(String value) {
			trade.customRule = value;
			return (T) this;
		}

		public TradeSignal build() {
			return new TradeSignal(this);
		}

	}
}
