package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.webdriver.DriverProvider;

public class IndicatorCreator extends BasePage {
	
	@FindBy(css = "[data-field='addIndicator']")
	WebElement addIndicatorButton;
	
	@FindBy(xpath = "//*[@id='selectError3_chzn']")
	WebElement indicatorsList;
	
	@FindBy(css = ".gwt-TextBox")
	WebElement indicatorName;
	
	@FindBy(xpath = "//td/select")
	WebElement symbolsList;
	
	@FindBy(css = "[data-field='addNewIndicatorDialogBody'] #modalSave")
	WebElement saveIndicatorButton;
	
	@FindBy(css = ".halflings-icon.remove")
	WebElement deleteIndictaorButton;
	
	@FindBy(css = ".ui-pnotify-text")
	WebElement confirmMessage;
	
	public IndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	
	protected IndicatorCreator(Builder builder) {
		super(builder.provider);
		
		addIndicatorButton.click();
		
		
		indicatorsList.click();

		String xpath = ".//*[@class='GOL5FQVBPI']/*[.='"
				+ builder.indicator.typeOfIndicator + "']";
		WebElement typeFromTheList = getDriver().findElement(By.xpath(xpath));

		Actions action = new Actions(getDriver());
		action.moveToElement(typeFromTheList).click().build().perform();

		String name = builder.indicator.typeOfIndicator + AccountPage.SystemCurrentTimeMillis;
		indicatorName.sendKeys(name);
		
		AccountPage.nameOfIndicator = name;

		symbolsList.click();
		String css = "[data-field='symbol'] [value='" + builder.indicator.symbol
				+ "']";
		WebElement symbolFromTheList = getDriver().findElement(
				By.cssSelector(css));

		action.moveToElement(symbolFromTheList).clickAndHold(symbolFromTheList)
				.release().build().perform();

		getDriver().findElement(
				By.xpath("//button[.='" + builder.indicator.timeFrame + "']"))
				.click();
		}
	
	public void saveIndicator() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		saveIndicatorButton.click();
				
		String text = "Indicator is saved with";
//		wait.until(ExpectedConditions.visibilityOf(confirmMessage));
		wait.until(ExpectedConditions.textToBePresentInElement(confirmMessage,
				text));
	}
	
	public void removeAllIndicators(){
		String css = ".label";
		String regex = ".*\\d{13}$";
		boolean isTrue = false;
				
		while(!isTrue){
			int i = 1;
			for (WebElement element : getDriver().findElements(By.cssSelector(css))){
				int size = getDriver().findElements(By.cssSelector(css)).size();
				if (element.getText().matches(regex)){
					
					String xpath = "//*[.='" + element.getText() + "']/../*[@class='todo-remove']";
					getDriver().findElement(By.xpath(xpath)).click();
					
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				}
				i++;
				if(i==size){
					isTrue=true;
				}
			logger.info("(for)isTrue:" + isTrue);
			}
			logger.info("(while)isTrue:" + isTrue);
		}
	}
	
	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public IndicatorsDescriptor indicator = new IndicatorsDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T typeOfIndicator(String value) {
			indicator.typeOfIndicator = value;
			return (T) this;
		}

		public T symbol(String value) {
			indicator.symbol = value;
			return (T) this;
		}

		public T timeFrame(String value) {
			indicator.timeFrame = value;
			return (T) this;
		}
		
		public T name(String value) {
			indicator.name = value;
			return (T) this;
		}
		public IndicatorCreator build() {
			return new IndicatorCreator(this);
		}

	}	
}
