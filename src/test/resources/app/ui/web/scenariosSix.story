Tests 6

Narrative:
In order to make sure deployment was successful
As a developer who did the deployment
I want to quickly validate that key application functionality was not broken


Scenario: Create Money Management Rule
Given I previosly logged in to account using 'carl' and 'pass' and validated that I landed on account page
When I am creating new strategy called 'strategy'
Then I am adding new money management rule with <name>

Examples:
|name	|type					|longPosOption	|shortPosOption	|longPosAmount	|shortPosAmount	|drawdownPercent	|drawdownEquity	|closePos	|pendingOrders	|deactivateTradingStrategies	|IPSlossAmount	|IPSlossPIPs	|IPTprofitAmount	|IPTprofitPIPs	|marginPercent	|openPosSameDir	|positionCB	|drawdownCB	|equityDrawDownCB	|amountCBloss	|amountCBprofit	|pipsCBloss	|pipsCBprofit	|
|rule	|Trade Size				|Variable Amount|Variable Amount|1				|1				|					|				|			|				|								|				|				|					|				|				|				|			|			|					|				|				|			|				|
|rule	|Account Stop Loss		|				|				|				|				|2					|2				|losing		|				|true							|				|				|					|				|				|				|true		|true		|true				|				|				|			|				|		
|rule	|Position Stop Loss		|				|				|				|				|					|				|			|				|								|3				|4				|					|6				|				|				|			|			|					|true			|				|true		|true			|			
|rule	|Maximum Exposure		|				|				|				|				|					|				|open		|				|								|				|				|					|				|8				|true			|			|			|					|				|				|			|				|