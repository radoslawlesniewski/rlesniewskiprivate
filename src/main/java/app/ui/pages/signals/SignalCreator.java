package app.ui.pages.signals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.IndicatorCreator;
import app.ui.pages.indicators.IndicatorsDescriptor;
import app.ui.pages.indicators.IndicatorCreator.Builder;
import app.ui.steps.AppUiSteps;
import app.ui.webdriver.DriverProvider;

public class SignalCreator extends BasePage {

	@FindBy(xpath = "//div[3]/div[2]//h2/a")
	WebElement addSignalButton;

	@FindBy(css = ".input-xlarge.focused")
	WebElement nameOfSignal;

	//@FindBy(css = ".switch-right.switch-danger")
	@FindBy(xpath = ".//*[@id='toggle-state-switch']/div")
	WebElement switchType;

	@FindBy(css = "#indicatorBtn")
	WebElement indicatorButton;

	@FindBy(css = "#symbolBtn")
	WebElement symbolButton;

	@FindBy(css = "#amountBtn")
	WebElement amountButton;

	@FindBy(css = "#logicBtn")
	WebElement logicButton;

	@FindBy(css = "#joinBtn")
	WebElement joinButton;

	@FindBy(css = ".btn.btn-primary.save-btn")
	WebElement saveSignalButton;

	@FindBy(css = ".ui-pnotify-text")
	WebElement confirmMessage;

	public SignalCreator(DriverProvider provider) {
		super(provider);
	}

	protected SignalCreator(Builder builder) {
		super(builder.provider);

		Actions action = new Actions(getDriver());
		String xpath;
		String option;
		WebElement typeFromTheList;
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);

		if (AppUiSteps.iterationSignalCreator == 0) {
			wait.until(ExpectedConditions.elementToBeClickable(addSignalButton));
			addSignalButton.click();

			nameOfSignal.sendKeys(builder.signal.name);

			if (!builder.signal.type.contains("Bear Signal"))
				//getDriver().findElement(By.xpath(switchType)).click();
				switchType.click();
		}

		if(!builder.signal.symbol1.isEmpty()){
			
			
		}
		
		if(!builder.signal.nameOfIndicator1.isEmpty()){
			
			
		}

		if (!builder.signal.join.isEmpty()) {
			wait.until(ExpectedConditions.elementToBeClickable(joinButton));
			joinButton.click();
			xpath = ".//*[@id='chozen_container__"
					+ AppUiSteps.iterationSignalCreator + "_chzn']/a/span";
			typeFromTheList = getDriver().findElement(By.xpath(xpath));
			action.moveToElement(typeFromTheList).click().build().perform();

			option = ".//*[@id='chozen_container__"
					+ AppUiSteps.iterationSignalCreator
					+ "_chzn']/div/ul/*[@class='GOL5FQVBEI'][.='"
					+ builder.signal.join + "']";
			typeFromTheList = getDriver().findElement(By.xpath(option));

			action.moveToElement(typeFromTheList).clickAndHold(typeFromTheList)
					.release().build().perform();
			AppUiSteps.iterationSignalCreator++;

		}

	}

	public void saveSignal() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);

		saveSignalButton.click();
		wait.until(ExpectedConditions.visibilityOf(confirmMessage));

	}

	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public SignalDescriptor signal = new SignalDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T name(String value) {
			signal.name = value;
			return (T) this;
		}

		public T type(String value) {
			signal.type = value;
			return (T) this;
		}

		public T nameOfIndicator1(String value) {
			signal.nameOfIndicator1 = value;
			return (T) this;
		}

		public T indicatorField1(String value) {
			signal.indicatorField1 = value;
			return (T) this;
		}

		public T logicOperator(String value) {
			signal.logicOperator = value;
			return (T) this;
		}

		public T symbol1(String value) {
			signal.symbol1 = value;
			return (T) this;
		}

		public T symbolOption1(String value) {
			signal.symbolOption1 = value;
			return (T) this;
		}

		public T join(String value) {
			signal.join = value;
			return (T) this;
		}

		public T nameOfIndicator2(String value) {
			signal.nameOfIndicator2 = value;
			return (T) this;
		}

		public T indicatorField2(String value) {
			signal.indicatorField2 = value;
			return (T) this;
		}

		public T symbol2(String value) {
			signal.symbol2 = value;
			return (T) this;
		}

		public T symbolOption2(String value) {
			signal.symbolOption2 = value;
			return (T) this;
		}

		public T amount(String value) {
			signal.amount = value;
			return (T) this;
		}

		public SignalCreator build() {
			return new SignalCreator(this);
		}

	}
}
