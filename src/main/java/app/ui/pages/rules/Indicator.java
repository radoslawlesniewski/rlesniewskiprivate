package app.ui.pages.rules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.signals.SignalDescriptor;
import app.ui.pages.rules.TradeTimingRuleCreator;
import app.ui.steps.AppUiSteps;
import app.ui.webdriver.DriverProvider;

public class Indicator extends BasePage {
	
	@FindBy(css = "#indicatorBtn")
	WebElement indicatorButton;
	
	@FindBy(css = "#amountBtn")
	WebElement amountButton;

	@FindBy(css = "#logicBtn")
	WebElement logicButton;
		
	@FindBy(xpath = "//span[.='..Choose Field..']/..")
	WebElement fieldsList;
	
	@FindBy(xpath = "//span[.='..Choose Indicator..']/..")
	WebElement indicatorsList;
	
	@FindBy(xpath = "//span[.='..Chose Operator..']/..")
	WebElement operatorsList;
	
	@FindBy(xpath = "//td[3]/div/div/input")
	WebElement amountInput;
	
	public Indicator(DriverProvider provider) {
		super(provider);
	}
	
	protected Indicator(Builder builder) {
		super(builder.provider);
		String xpath = null;
		Actions action = new Actions(getDriver());
		indicatorButton.click();
		
		indicatorsList.click();
		
		xpath = ".//*[@class='GOL5FQVBEI'][.='"
				+ builder.indicator.nameOfIndicator + "']";
		WebElement indicatorFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(indicatorFromTheList).click().build().perform();

		fieldsList.click();
		
		xpath = ".//*[@class='GOL5FQVBPI']/li[.='"
				+ builder.indicator.indicatorField + "']";
		WebElement fieldFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(fieldFromTheList).click().build().perform();
		
		logicButton.click();		
		operatorsList.click();
		
		xpath = ".//*[@class='GOL5FQVBPI']/li[.='"
				+ builder.indicator.logicOperator + "']";
		WebElement operatorFromTheList = getDriver().findElement(By.xpath(xpath));

		action.moveToElement(operatorFromTheList).click().build().perform();
		
		amountButton.click();
		amountInput.sendKeys(builder.indicator.amount);
		
	}
	
	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public RuleDescriptor indicator = new RuleDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T nameOfIndicator(String value) {
			indicator.nameOfIndicator = value;
			return (T) this;
		}

		public T indicatorField(String value) {
			indicator.indicatorField = value;
			return (T) this;
		}

		public T logicOperator(String value) {
			indicator.logicOperator = value;
			return (T) this;
		}
		
		public T amount(String value) {
			indicator.amount = value;
			return (T) this;
		}
		
		public Indicator build() {
			return new Indicator(this);
		}

	}

}
