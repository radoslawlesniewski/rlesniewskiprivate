package app.ui.pages.rules;

import org.openqa.selenium.WebDriver;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class MultipleTradeHandling extends BasePage {

	@FindBy(xpath = "//div[3]//div[1]/div[2]/div//div[2]/div/button[2]")
	WebElement multipleTradeHandlingButton;
	
	@FindBy(xpath = ".//*[@id='additionalSignalPanel']/div/div[2]/div/button[1]")
	WebElement additionalIgnoreButton;

	@FindBy(xpath = ".//*[@id='additionalSignalPanel']/div/div[2]/div/button[2]" )
	WebElement openAdditionalPositionButton;
	
	@FindBy(xpath = ".//*[@id='oppositeSignalPanel']/div/div[2]/div/button[1]")
	WebElement oppositeIgnoreButton;
	
	@FindBy(xpath = ".//*[@id='oppositeSignalPanel']/div/div[2]/div/button[2]" )
	WebElement closePositionButton;
	
	@FindBy(xpath = ".//*[@id='oppositeSignalPanel']/div/div[2]/div/button[3]")
	WebElement closeAndReverseButton;
	
	@FindBy(xpath = ".//*[@id='oppositeSignalPanel']/div/div[2]/div/button[4]")
	WebElement hedgeButton;
	
	
	public MultipleTradeHandling(DriverProvider provider) {
		super(provider);
	}

	protected MultipleTradeHandling(Builder builder) {
		super(builder.provider);

		multipleTradeHandlingButton.click();
		
		switch(builder.trade.sameDirectionSignal){
		case "Ignore" : 
			additionalIgnoreButton.click();
			break;
		case "Open additional position" :
			openAdditionalPositionButton.click();
			break;
		}
		
		switch(builder.trade.oppositeDirectionSignal){
		case "Ignore" : 
			oppositeIgnoreButton.click();
			break;
		case "Close existing position" :
			closePositionButton.click();
			break;
		case "Close and Reverse" :
			closeAndReverseButton.click();
			break;
		case "Hedge" :
			hedgeButton.click();
			break;
		}
		
	
	}

	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public RuleDescriptor trade = new RuleDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T sameDirection(String value) {
			trade.sameDirectionSignal = value;
			return (T) this;
		}

		public T oppositeDirection(String value) {
			trade.oppositeDirectionSignal = value;
			return (T) this;
		}

		public MultipleTradeHandling build() {
			return new MultipleTradeHandling(this);
		}

	}
}
