package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

public class ParabolicSarIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[1]//div[3]/input")
	WebElement step;
	
	@FindBy(xpath = "//tr[2]//div[3]/input")
	WebElement maximum;
			
	public ParabolicSarIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	
	private ParabolicSarIndicatorCreator(Builder builder) {
		super(builder);
		step.clear();
		step.sendKeys(builder.indicatorDescriptor.step);
		maximum.clear();
		maximum.sendKeys(builder.indicatorDescriptor.maximum);
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		public Builder step(String value) {
			indicatorDescriptor.step = value;
			return this;
		}
		
		public Builder maximum(String value) {
			indicatorDescriptor.maximum = value;
			return this;
		}
		
		public ParabolicSarIndicatorCreator build(){
			return new ParabolicSarIndicatorCreator(this);
		}
	}

}
