package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

public class IchimokuIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[1]//div[3]/input")
	WebElement tenkanSen;
	
	@FindBy(xpath = "//tr[2]//div[3]/input")
	WebElement kijunSen;
	
	@FindBy(xpath = "//tr[3]//div[3]/input")
	WebElement senkouSpanB;
	
	public IchimokuIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	

	private IchimokuIndicatorCreator(Builder builder) {
		super(builder);
		
		tenkanSen.clear();
		tenkanSen.sendKeys(builder.indicatorDescriptor.tenkanSen);
		kijunSen.clear();
		kijunSen.sendKeys(builder.indicatorDescriptor.kijunSen);
		senkouSpanB.clear();
		senkouSpanB.sendKeys(builder.indicatorDescriptor.senkouSpanB);
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		public Builder tenkanSen(String value) {
			indicatorDescriptor.tenkanSen = value;
			return this;
		}
		
		public Builder kijunSen(String value) {
			indicatorDescriptor.kijunSen = value;
			return this;
		}
		
		public Builder senkouSpanB(String value) {
			indicatorDescriptor.senkouSpanB = value;
			return this;
		}
		
		
		public IchimokuIndicatorCreator build(){
			return new IchimokuIndicatorCreator(this);
		}
	}

}
