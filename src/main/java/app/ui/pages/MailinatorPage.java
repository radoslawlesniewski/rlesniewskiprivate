package app.ui.pages;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.seleniumemulation.WaitForPageToLoad;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.pages.Page;
import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class MailinatorPage extends BasePage implements Page {
	@FindBy(css = ".btn.btn-success")
	public WebElement chectItButton;
	
	@FindBy(css = "#inboxfield")
	public WebElement inboxField;
	
	@FindBy(css = ".subject.ng-binding")
	public WebElement accountInformationButton;
	
	
	public MailinatorPage(DriverProvider provider) {
		super(provider);
		PageFactory.initElements(getDriver(), this);
	}

	@Override
	public void init() {
		getDriver().manage().deleteAllCookies();
		getDriver().get("http://mailinator.com/");
		getDriver().manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		System.currentTimeMillis();
	}
	
	public void typeUserLogin(String user){
		inboxField.sendKeys(user);
		chectItButton.click();
	}

	public String retrievePassword() {
		accountInformationButton.click();

		String textField = ".mailview > p:nth-child(2)";
		WebElement emailTextField = getDriver().findElement(
				By.cssSelector(textField));

		String temp = emailTextField.getText().substring(
				emailTextField.getText().indexOf("Password"));
		String password = temp.substring(temp.indexOf(":")+1).trim();
		logger.info(password);
		return password;
	}
	
	public boolean subjectOfMessage(String subject){
		while(!accountInformationButton.getText().toLowerCase().contains(subject)){
			/*try {
				getDriver().manage().timeouts().wait(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			getDriver().navigate().refresh();
		}
		return accountInformationButton.getText().toLowerCase().contains(subject);
	}
	
	
}
