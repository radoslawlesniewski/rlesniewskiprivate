package app.ui.pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.seleniumemulation.WaitForCondition;
import org.openqa.selenium.internal.seleniumemulation.WaitForPageToLoad;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class AccountPage extends BasePage {
	Logger logger = LoggerFactory.getLogger(AccountPage.class);
	
	@FindBy(css = ".brand>span")
	WebElement title;
	
	@FindBy(css = "#myStrategies>a")
	WebElement myStrategiesTab;
	
	@FindBy(css = "#appendedInputButton")
	WebElement strategyName;
	
	@FindBy(css = "button[type='button']")
	WebElement goButton;
	
	@FindBy(css = "[data-field='bullet1_addMarket']")
	WebElement addMarketButton;
	
	@FindBy(css = "[data-field='symbolsListBox']")
	WebElement marketsList;
	
	@FindBy(css = "#modalSave")
	WebElement saveButton;
	
	@FindBy(xpath = "//div[@class='span6'][1]//li[1]/a")
	WebElement confirmMessage;
	
	public AccountPage(DriverProvider provider) {
		super(provider);
		PageFactory.initElements(getDriver(), this);
	}
	
	public void validatePage() {
		WebDriverWait wait = new WebDriverWait(getDriver(),20);
		wait.until(ExpectedConditions.visibilityOf(title));
		Assert.assertTrue(title.getText().equals("Magick"));
	}
	
	public void createNewStrategy(String name) {
		WebDriverWait wait = new WebDriverWait(getDriver(),20);
		String css = "#myStrategies";
//		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		myStrategiesTab.click();
		
		css = "#appendedInputButton";
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		strategyName.sendKeys(name + System.currentTimeMillis());
		goButton.click();
	}
	
	public void addMarket(String market) {
		WebDriverWait wait = new WebDriverWait(getDriver(),20);
		String css = "[data-field='bullet1_addMarket']";
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		addMarketButton.click();
		
		css = ".gwt-ListBox";
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		Select select = new Select(marketsList);
		select.selectByValue(market);
		saveButton.click();
		
		String xpath = "//div[@class='span6'][1]//li[1]/a";
		wait.until(ExpectedConditions.textToBePresentInElement(By.xpath(xpath), "Symbol Added"));
		Assert.assertTrue(confirmMessage.getText().contains("Symbol Added"));
	}
	
	
}
