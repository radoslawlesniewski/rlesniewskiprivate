package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

public class DeMarkerIndicatorCreator extends IndicatorCreator{
	
	@FindBy(xpath = "//tr[1]//div[3]/input")
	WebElement period;
	
	public DeMarkerIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	
	private DeMarkerIndicatorCreator(Builder builder) {
		super(builder);
		
		period.clear();
		period.sendKeys(builder.indicatorDescriptor.period);
		
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		
		public Builder period(String value) {
			indicatorDescriptor.period = value;
			return this;
		}
		
		public DeMarkerIndicatorCreator build(){
			return new DeMarkerIndicatorCreator(this);
		}
	}
}
