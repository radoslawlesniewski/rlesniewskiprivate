package app.ui.pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.seleniumemulation.WaitForCondition;
import org.openqa.selenium.internal.seleniumemulation.WaitForPageToLoad;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class AccountPage extends BasePage {
	Logger logger = LoggerFactory.getLogger(AccountPage.class);

	public static long SystemCurrentTimeMillis;
	public static String nameOfIndicator;

	@FindBy(css = "title")
	WebElement title;
	
	@FindBy(css = ".nav.pull-right.btn.btn-medium.btn-danger.btn-create-new-strategy")
	WebElement createStrategyButton;

	@FindBy(css = "#myStrategies>a")
	WebElement myStrategiesTab;

	@FindBy(css = "#appendedInputButton")
	WebElement strategyName;

	@FindBy(css = "button[type='button']")
	WebElement goButton;

	@FindBy(css = "[data-field='bullet1_addMarket']")
	WebElement addMarketButton;

	@FindBy(css = "[data-field='symbolsListBox']")
	WebElement marketsList;

	@FindBy(css = "#modalSave")
	WebElement saveButton;

	@FindBy(xpath = "//div[@class='span6'][1]//li[1]/a")
	WebElement confirmMessage;
	
	@FindBy(css = ".dropdown-toggle")
	WebElement menuButton;
	
	@FindBy(css = "[data-field = logout]")
	WebElement logOutButton;
	
	@FindBy(css = ".btn.btn-link")
	List <WebElement> strategiesList;

	public AccountPage(DriverProvider provider) {
		super(provider);
		PageFactory.initElements(getDriver(), this);
	}

	public void validatePage() {
		Assert.assertTrue(getDriver().getTitle().equals("Magick"));
	}

	public void createNewStrategy(String name) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		String css = "#myStrategies";
		//myStrategiesTab.click();
		
		createStrategyButton.click();
		css = "#appendedInputButton";
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		SystemCurrentTimeMillis = System.currentTimeMillis();
		strategyName.sendKeys(name + SystemCurrentTimeMillis);
		goButton.click();

	}

	public void addMarket(String market) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		String css = "[data-field='bullet1_addMarket']";
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		addMarketButton.click();

		css = ".gwt-ListBox";
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
		Select select = new Select(marketsList);
		select.selectByValue(market);
		saveButton.click();

		String xpath = "//div[@class='span6'][1]//li[1]/a";
		wait.until(ExpectedConditions.textToBePresentInElement(By.xpath(xpath),
				"Symbol Added"));
		Assert.assertTrue(confirmMessage.getText().contains("Symbol Added"));
	}

	public boolean checkIfMessageIsShown() {

		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOf(confirmMessage));
		return true;

	}

	public boolean checkMessageProperty(String value) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOf(confirmMessage));
		return confirmMessage.getText().contains(value);
	}
	

	public void goToFirstStrategy() {
		myStrategiesTab.click();
		strategiesList.get(0).click();
	}
	
	public void logOut() {
		menuButton.click();
		logOutButton.click();
	}

}
