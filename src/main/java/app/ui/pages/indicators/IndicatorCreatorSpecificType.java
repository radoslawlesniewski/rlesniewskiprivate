package app.ui.pages.indicators;

import app.ui.pages.indicators.IndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.StorageHolder.Type;

import app.ui.common.BasePage;

public class IndicatorCreatorSpecificType extends BasePage {

	AlligatorIndicatorCreator alligatorIndicatorCreator;
	BollingerBandsIndicatorCreator bollingerBandsIndicatorCreator;
	BollingerBandsSecondTypeIndicatorCreator bollingerBandsSecondTypeIndicatorCreator;
	DeMarkerIndicatorCreator deMarkerIndicatorCreator;
	EnvelopesIndicatorCreator envelopesIndicatorCreator;
	IchimokuIndicatorCreator ichimokuIndicatorCreator;
	MACDIndicatorCreator macdIndicatorCreator;
	ParabolicSarIndicatorCreator parabolicSarIndicatorCreator;
	StochasticOscillilatorIndicatorCreator stochasticOscillilatorIndicatorCreator;
	IndicatorCreator indicatorCreator;
	EnvelopesSecondTypeIndicatorCreator envelopesSecondTypeIndicatorCreator; 
	MovingAverageIndicatorCreator movingAverageIndicatorCreator;

	public IndicatorCreatorSpecificType(DriverProvider provider) {
		super(provider);
	}

	protected IndicatorCreatorSpecificType(Builder builder,
			String typeOfIndicator) {
		super(builder.provider);

		switch (typeOfIndicator) {

		case "Alligator":
			alligatorIndicatorCreator = (AlligatorIndicatorCreator) new AlligatorIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.jawsPeriod(builder.indicator.jawsPeriod)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.teethPeriod(builder.indicator.teethPeriod)
					.lipsPeriod(builder.indicator.lipsPeriod)
					.jawsShift(builder.indicator.jawsShift)
					.teethShift(builder.indicator.teethShift)
					.lipsShift(builder.indicator.lipsShift)
					.maMethod(builder.indicator.maMethod).build();
			alligatorIndicatorCreator = new AlligatorIndicatorCreator(
					getProvider());
			alligatorIndicatorCreator.saveIndicator();
			break;

		case "Accelerator Oscillator":
			indicatorCreator = new IndicatorCreator.Builder(getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name).build();
			indicatorCreator = new IndicatorCreator(getProvider());
			indicatorCreator.saveIndicator();
			break;

		case "Accumulation Distribution":

			indicatorCreator = new IndicatorCreator.Builder(getProvider())
					.typeOfIndicator(typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name).build();
			indicatorCreator = new IndicatorCreator(getProvider());
			indicatorCreator.saveIndicator();
			break;

		case "Average Directional Movement Index":
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviations(builder.indicator.deviations).build();
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator(
					getProvider());
			bollingerBandsIndicatorCreator.saveIndicator();
			break;

		case "Average True Range":
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.period(builder.indicator.period).build();
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator(
					getProvider());
			deMarkerIndicatorCreator.saveIndicator();
			break;

		case "Awesome Oscillator":
			indicatorCreator = new IndicatorCreator.Builder(getProvider())
					.typeOfIndicator(typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name).build();
			indicatorCreator = new IndicatorCreator(getProvider());
			indicatorCreator.saveIndicator();
			break;

		case "Bears Power":
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviations(builder.indicator.deviations).build();
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator(
					getProvider());
			bollingerBandsIndicatorCreator.saveIndicator();
			break;

		case "Bollinger Bands":
			bollingerBandsSecondTypeIndicatorCreator =  new BollingerBandsSecondTypeIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviations(builder.indicator.deviations).build();
			bollingerBandsSecondTypeIndicatorCreator = new BollingerBandsSecondTypeIndicatorCreator(
					getProvider());
			bollingerBandsSecondTypeIndicatorCreator.saveIndicator();
			break;

		case "Bulls Power":
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviations(builder.indicator.deviations).build();
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator(
					getProvider());
			bollingerBandsIndicatorCreator.saveIndicator();
			break;

		case "Commodity Channel Index":
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviations(builder.indicator.deviations).build();
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator(
					getProvider());
			bollingerBandsIndicatorCreator.saveIndicator();
			break;

		case "De Marker":
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.period(builder.indicator.period).build();
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator(
					getProvider());
			deMarkerIndicatorCreator.saveIndicator();
			break;

		case "Envelopes":
			envelopesIndicatorCreator = new EnvelopesIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.maMethod(builder.indicator.maMethod)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviation(builder.indicator.deviation).build();
			envelopesIndicatorCreator = new EnvelopesIndicatorCreator(
					getProvider());
			envelopesIndicatorCreator.saveIndicator();
			break;

		case "Force Index":
			envelopesSecondTypeIndicatorCreator = new EnvelopesSecondTypeIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.maMethod(builder.indicator.maMethod)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.build();
			envelopesSecondTypeIndicatorCreator = new EnvelopesSecondTypeIndicatorCreator(
					getProvider());
			envelopesSecondTypeIndicatorCreator.saveIndicator();
			break;

		case "Fractals":
			indicatorCreator = new IndicatorCreator.Builder(getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name).build();
			indicatorCreator = new IndicatorCreator(getProvider());
			indicatorCreator.saveIndicator();
			break;

		case "Gator Oscillator":
			alligatorIndicatorCreator = (AlligatorIndicatorCreator) new AlligatorIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.jawsPeriod(builder.indicator.jawsPeriod)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.teethPeriod(builder.indicator.teethPeriod)
					.lipsPeriod(builder.indicator.lipsPeriod)
					.jawsShift(builder.indicator.jawsShift)
					.teethShift(builder.indicator.teethShift)
					.lipsShift(builder.indicator.lipsShift)
					.maMethod(builder.indicator.maMethod).build();
			alligatorIndicatorCreator = new AlligatorIndicatorCreator(
					getProvider());
			alligatorIndicatorCreator.saveIndicator();
			break;

		case "Ichimoku":
			ichimokuIndicatorCreator = new IchimokuIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.tenkanSen(builder.indicator.tenkanSen)
					.kijunSen(builder.indicator.kijunSen)
					.senkouSpanB(builder.indicator.senkouSpanB).build();
			ichimokuIndicatorCreator = new IchimokuIndicatorCreator(
					getProvider());
			ichimokuIndicatorCreator.saveIndicator();
			break;

		case "MACD":
			macdIndicatorCreator = new MACDIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.fastEMA(builder.indicator.fastEMA)
					.slowEMA(builder.indicator.slowEMA)
					.macdSMA(builder.indicator.macdSMA).build();
			macdIndicatorCreator = new MACDIndicatorCreator(getProvider());
			macdIndicatorCreator.saveIndicator();
			break;

		case "Market Facilitation Index":
			indicatorCreator = new IndicatorCreator.Builder(getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name).build();
			indicatorCreator = new IndicatorCreator(getProvider());
			indicatorCreator.saveIndicator();
			break;

		case "Momentum":
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviations(builder.indicator.deviations).build();
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator(
					getProvider());
			bollingerBandsIndicatorCreator.saveIndicator();
			break;

		case "Money Flow Index":
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.period(builder.indicator.period).build();
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator(
					getProvider());
			deMarkerIndicatorCreator.saveIndicator();
			break;

		case "Moving Average":
			movingAverageIndicatorCreator = new MovingAverageIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.maMethod(builder.indicator.maMethod)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.build();
			movingAverageIndicatorCreator = new MovingAverageIndicatorCreator(
					getProvider());
			movingAverageIndicatorCreator.saveIndicator();
			break;

		case "Moving Average Of Oscillator":
			macdIndicatorCreator = new MACDIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.fastEMA(builder.indicator.fastEMA)
					.slowEMA(builder.indicator.slowEMA)
					.macdSMA(builder.indicator.macdSMA).build();
			macdIndicatorCreator = new MACDIndicatorCreator(getProvider());
			macdIndicatorCreator.saveIndicator();
			break;

		case "On Balance Volume":
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviations(builder.indicator.deviations).build();
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator(
					getProvider());
			bollingerBandsIndicatorCreator.saveIndicator();
			break;

		case "Parabolic SAR":
			parabolicSarIndicatorCreator = new ParabolicSarIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name).step(builder.indicator.step)
					.maximum(builder.indicator.maximum).build();
			parabolicSarIndicatorCreator = new ParabolicSarIndicatorCreator(
					getProvider());
			parabolicSarIndicatorCreator.saveIndicator();
			break;

		case "Relative Strength Index":
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.deviations(builder.indicator.deviations).build();
			bollingerBandsIndicatorCreator = new BollingerBandsIndicatorCreator(
					getProvider());
			bollingerBandsIndicatorCreator.saveIndicator();
			break;

		case "Relative Vigor Index":
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.period(builder.indicator.period).build();
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator(
					getProvider());
			deMarkerIndicatorCreator.saveIndicator();
			break;

		case "Standard Deviation":
			movingAverageIndicatorCreator = new MovingAverageIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.maMethod(builder.indicator.maMethod)
					.applyTo(builder.indicator.applyTo)
					.period(builder.indicator.period)
					.shift(builder.indicator.shift)
					.build();
			movingAverageIndicatorCreator = new MovingAverageIndicatorCreator(
					getProvider());
			movingAverageIndicatorCreator.saveIndicator();
			break;

		case "Stochastic Oscillator":
			stochasticOscillilatorIndicatorCreator = new StochasticOscillilatorIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.maMethod(builder.indicator.maMethod)
					.dPeriod(builder.indicator.dPeriod)
					.kPeriod(builder.indicator.kPeriod)
					.slowing(builder.indicator.slowing)
					.priceField(builder.indicator.priceField).build();
			stochasticOscillilatorIndicatorCreator = new StochasticOscillilatorIndicatorCreator(
					getProvider());
			stochasticOscillilatorIndicatorCreator.saveIndicator();
			break;

		case "Williams Percent Range":
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator.Builder(
					getProvider())
					.typeOfIndicator(builder.indicator.typeOfIndicator)
					.symbol(builder.indicator.symbol)
					.timeFrame(builder.indicator.timeFrame)
					.name(builder.indicator.name)
					.period(builder.indicator.period).build();
			deMarkerIndicatorCreator = new DeMarkerIndicatorCreator(
					getProvider());
			deMarkerIndicatorCreator.saveIndicator();
			break;

		}
	}

	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public IndicatorsDescriptor indicator = new IndicatorsDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T typeOfIndicator(String value) {
			indicator.typeOfIndicator = value;
			return (T) this;
		}

		public T symbol(String value) {
			indicator.symbol = value;
			return (T) this;
		}

		public T timeFrame(String value) {
			indicator.timeFrame = value;
			return (T) this;
		}

		public T name(String value) {
			indicator.name = value;
			return (T) this;
		}

		public T jawsPeriod(String value) {
			indicator.jawsPeriod = value;
			return (T) this;
		}

		public T teethPeriod(String value) {
			indicator.teethPeriod = value;
			return (T) this;
		}

		public T lipsPeriod(String value) {
			indicator.lipsPeriod = value;
			return (T) this;
		}

		public T jawsShift(String value) {
			indicator.jawsShift = value;
			return (T) this;
		}

		public T teethShift(String value) {
			indicator.teethShift = value;
			return (T) this;
		}

		public T lipsShift(String value) {
			indicator.lipsShift = value;
			return (T) this;
		}

		public T applyTo(String value) {
			indicator.applyTo = value;
			return (T) this;
		}

		public T maMethod(String value) {
			indicator.maMethod = value;
			return (T) this;
		}

		public T period(String value) {
			indicator.period = value;
			return (T) this;
		}

		public T shift(String value) {
			indicator.shift = value;
			return (T) this;
		}

		public T deviations(String value) {
			indicator.deviations = value;
			return (T) this;
		}

		public T deviation(String value) {
			indicator.deviation = value;
			return (T) this;
		}

		public T tenkanSen(String value) {
			indicator.tenkanSen = value;
			return (T) this;
		}

		public T kijunSen(String value) {
			indicator.kijunSen = value;
			return (T) this;
		}

		public T senkouSpanB(String value) {
			indicator.senkouSpanB = value;
			return (T) this;
		}

		public T fastEMA(String value) {
			indicator.fastEMA = value;
			return (T) this;
		}

		public T slowEMA(String value) {
			indicator.slowEMA = value;
			return (T) this;
		}

		public T macdSMA(String value) {
			indicator.macdSMA = value;
			return (T) this;
		}

		public T step(String value) {
			indicator.step = value;
			return (T) this;
		}

		public T maximum(String value) {
			indicator.maximum = value;
			return (T) this;
		}

		public T dPeriod(String value) {
			indicator.dPeriod = value;
			return (T) this;
		}

		public T kPeriod(String value) {
			indicator.kPeriod = value;
			return (T) this;
		}

		public T slowing(String value) {
			indicator.slowing = value;
			return (T) this;
		}

		public T priceField(String value) {
			indicator.priceField = value;
			return (T) this;
		}

		public IndicatorCreatorSpecificType build() {
			return new IndicatorCreatorSpecificType(this,
					indicator.typeOfIndicator);
		}

	}

}
