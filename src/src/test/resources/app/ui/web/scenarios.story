Tests 1

Meta:
@group scenarios

Narrative:
In order to make sure deployment was successful
As a developer who did the deployment
I want to quickly validate that key application functionality was not broken

!-- Scenario: Log in to account
!-- Given I am on the Magick login page
!-- When I am loging in using following 'carl' username and 'pass' password
!-- Then I am landing on my account page
!-- 
!-- Scenario: Create strategy with some Marketailed=
!-- Given I previosly logged in to account using '<username>' and '<password>' and validated that I landed on account page
!-- When I am creating new strategy called '<name>'
!-- Then I am adding '<market>' market
!-- And I am adding Indicator with following '<type>' type, '<symbol>' symbol and '<timeFrame>' time frame
!-- 
!-- Examples:
!-- |username	|password	|name		|market|type		|symbol|timeFrame	|
!-- |carl		|pass		|myStrategy	|EURUSD|Alligator	|EURUSD|M15			|

Scenario: Create strategy with some Indicator
Given I previosly logged in to account using '<username>' and '<password>' and validated that I landed on account page
When I am creating new strategy called '<name>' and adding new Indicator
Then I am deleting this Indicator and checking if operation was succesful

Examples:
|username	|password	|name		|type		|symbol|timeFrame	|jawsPeriod	|
|carl		|pass		|MyStrategy	|Alligator	|EURUSD|M15			|16			|

Scenario: Create strategy with valid Indicator
Given I previosly logged in to account using '<username>' and '<password>' and created Strategy called 'myStrategy'
When I am creating new Indicator  (<type>, <symbol>, <timeFrame>) without name (also optionally: '<jawsPeriod>' jawsPeriod)
Then I am checking if message with error is visible

Examples:
|username	|password	|type						|symbol|timeFrame	|jawsPeriod	|
|carl		|pass		|Market Facilitation Index	|EURUSD|M15			|16			|


Scenario: Create strategy with another valid Indicator
Given I previosly logged in to account using '<username>' and '<password>' and created Strategy called 'myStrategy'
When I am creating new Indicator without symbol
Then I get error message about Indicator

Examples:
|username	|password	|type		|symbol|timeFrame	|
|carl		|pass		|Alligator	|EURUSD|M15			|

Scenario: Create strategy with number-name Indicator
Given I previosly logged in to account using '<username>' and '<password>' and created Strategy called 'myStrategy'
When I am adding Indicators of every type with names: indicator#$%$%&%^@
Then I am testing wchich Indicators can be named with letters and special marks

Examples:
|username	|password	|type		|symbol|timeFrame	|
|carl		|pass		|Alligator	|EURUSD|M15			|
