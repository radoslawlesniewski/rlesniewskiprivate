package app.ui.pages.indicators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import app.ui.webdriver.DriverProvider;

public class AlligatorIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[1]//div[3]/input")
	WebElement jawsPeriod;
	
	public AlligatorIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	
	private AlligatorIndicatorCreator(Builder builder) {
		super(builder);
		jawsPeriod.clear();
		jawsPeriod.sendKeys(builder.indicatorDescriptor.jawsPeriod);
		String temp = "";
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		public Builder jawsPeriod(String value) {
			indicatorDescriptor.jawsPeriod = value;
			return this;
		}
		
		public AlligatorIndicatorCreator build(){
			return new AlligatorIndicatorCreator(this);
		}
	}
	
	
}
