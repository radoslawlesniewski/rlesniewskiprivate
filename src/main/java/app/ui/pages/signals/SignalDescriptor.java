package app.ui.pages.signals;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class SignalDescriptor {
	
	public String name;
	public String type;
	public String nameOfIndicator1;
	public String indicatorField1;
	public String logicOperator;
	public String symbol1;
	public String symbolOption1;
	public String join;
	public String nameOfIndicator2;
	public String indicatorField2;
	public String symbol2;
	public String symbolOption2;
	public String amount;

}
