package app.ui.pages.rules;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class RuleDescriptor {
	
	public String name;
	public String type;
	public String nameOfIndicator;
	public String indicatorField;
	public String logicOperator;
	public String symbol;
	public String symbolOption;
	public String join;
	public String amount;
	public String bullBearSignal;
	public String ruleType;
	public String sameDirectionSignal;
	public String oppositeDirectionSignal;
	public String customRule;
}
