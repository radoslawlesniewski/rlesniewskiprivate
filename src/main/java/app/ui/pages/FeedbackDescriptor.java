package app.ui.pages;

public class FeedbackDescriptor {
	public String question;
	public String details;
	public String name;
	public String emailAddress;
}
