package app.ui.tradingAccount;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.feedback.Feedback;
import app.ui.feedback.FeedbackDescriptor;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.IndicatorCreator;
import app.ui.pages.indicators.IndicatorsDescriptor;
import app.ui.pages.indicators.IndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;
import app.ui.webdriver.listeners.MyEventListener;

public class TradingAccountCreator extends BasePage {
	
	@FindBy(xpath = ".//*[@id='brokers_chzn']/a")
	WebElement brokers;
	
	@FindBy(css = "#label")
	WebElement descriptionField;

	@FindBy(css = "#username")
	WebElement usernameField;
	
	@FindBy(css = "#password")
	WebElement passwordField;
	
	@FindBy(xpath = ".//*[@id='servers_chzn']/a")
	WebElement servers;
	
	@FindBy(css = "#save")
	WebElement saveButton;
	
	@FindBy(xpath = ".//*[@id='myAccount']/a")
	WebElement myAccount;
	
	@FindBy(xpath = ".//*[@id='addTraderAccount']/a")
	WebElement traderAccount;
	
	public TradingAccountCreator(DriverProvider provider) {
		super(provider);
	}

	protected TradingAccountCreator(Builder builder) {
		super(builder.provider);
		
		myAccount.click();
		traderAccount.click();
		descriptionField.sendKeys(builder.account.description);
		
		brokers.click();
		String xpath = ".//*[@class='GOL5FQVBPI']/*[.='"+ builder.account.broker +"']";
		WebElement typeOneFromTheList = getDriver().findElement(By.xpath(xpath));

		Actions action = new Actions(getDriver());
		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
		
		servers.click();
		xpath = ".//*[@class='GOL5FQVBOI']/*[@class='GOL5FQVBPI']/*[.='"+ builder.account.server +"']";
		typeOneFromTheList = getDriver().findElement(By.xpath(xpath));

		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
		
		usernameField.sendKeys(builder.account.username);
		passwordField.sendKeys(builder.account.password);
		
		saveButton.click();
		
	}
	
	public void searchOnListAndDeleteAccount(String description) {
		
		String xpath = ".//*[@id='"+description+"']/td[1]";
		MyEventListener.addIgnoredElement("By.xpath: .//*[@id='"+description+"']/td[1]");
		
		if(getDriver().findElement(By.xpath(xpath)).isDisplayed()){
			xpath = ".//*[@id='"+description+"']/td[4]/a[2]";
			getDriver().findElement(By.xpath(xpath)).click();
		}
		else{
			System.out.println("Account not found");
		}
	}
	
	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public TradingAccountDescriptor account = new TradingAccountDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T description(String value) {
			account.description = value;
			return (T) this;
		}

		public T broker(String value) {
			account.broker = value;
			return (T) this;
		}

		public T server(String value) {
			account.server = value;
			return (T) this;
		}

		public T username(String value) {
			account.username = value;
			return (T) this;
		}

		public T password(String value) {
			account.password = value;
			return (T) this;
		}
		
		public TradingAccountCreator build() {
			return new TradingAccountCreator(this);
		}

	}


}
