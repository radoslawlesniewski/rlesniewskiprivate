package app.ui.pages.rules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class MoneyManagementRuleCreator extends BasePage {

	@FindBy(xpath = "//div[3]/div[4]/div[1]/h2/a")
	WebElement addRuleButton;

	@FindBy(xpath = "//div[3]//div[2]/input")
	WebElement nameOfRule;

	@FindBy(xpath = ".//*[@data-field='tradeOptionLong']")
	WebElement LongPositions;

	@FindBy(xpath = ".//*[@data-field='tradeOptionShort']")
	WebElement ShortPositions;

	@FindBy(css = ".btn.btn-primary.save-btn")
	WebElement saveRuleButton;

	@FindBy(css = ".ui-pnotify-text")
	WebElement confirmMessage;

	@FindBy(xpath = "//div[3]//div[2]//div/button[1]")
	WebElement tradeSizeButton;

	@FindBy(xpath = "//div[3]//div[2]//div/button[2]")
	WebElement accountStopLossButton;

	@FindBy(xpath = "//div[3]//div[2]//div/button[3]")
	WebElement positionStopLossButton;

	@FindBy(xpath = "//div[3]//div[2]//div/button[4]")
	WebElement maximumExposureButton;

	@FindBy(xpath = ".//*[@data-field='tradeOptionLong']")
	WebElement tradeOptionLongList;

	@FindBy(xpath = ".//*[@data-field='tradeOptionShort']")
	WebElement tradeOptionShortList;
	
	@FindBy(xpath = ".//*[@data-field='tradeOptionLong']/option[1]")
	WebElement fixedAmountLongPositions;
	
	@FindBy(xpath = ".//*[@data-field='tradeOptionLong']/option[2]")
	WebElement variableAmountLongPositions;
	
	@FindBy(xpath = ".//*[@data-field='tradeOptionShort']/option[1]")
	WebElement fixedAmountShortPositions;
	
	@FindBy(xpath = ".//*[@data-field='tradeOptionShort']/option[2]")
	WebElement variableAmountShortPositions;
	
	@FindBy(xpath = ".//*[@id='content']//div[1]/div/div[3]/input")
	WebElement longPositionsAmountInput;
	
	@FindBy(xpath = ".//*[@id='content']//div[2]/div/div[3]/input")
	WebElement shortPositionsAmountInput;
	
	@FindBy(xpath = "//div[3]//tr[1]/td[2]/input")
	WebElement drawdownPercentInput;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-7']")
	WebElement drawdownCheckBox;
	
	@FindBy(xpath = "//div[3]//tr[2]/td[2]/input")
	WebElement equityDrawdownInput;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-8']")
	WebElement equityDrawdownCheckBox;
	
	@FindBy(xpath = ".//*[@data-field='positionLB']")
	WebElement positionList;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-9']")
	WebElement positionCheckBox;
	
	@FindBy(xpath = ".//*[@data-field='positionLB']/option[1]")
	WebElement closeOpenPositionsField;
	
	@FindBy(xpath = ".//*[@data-field='positionLB']/option[2]")
	WebElement closeProfitablePositionsField;
	
	@FindBy(xpath = ".//*[@data-field='positionLB']/option[3]")
	WebElement closeLosingPositionsField;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-10']")
	WebElement pendingOrdersCheckBox;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-11']")
	WebElement deactivateTradingStrategiesCheckBox;
	
	@FindBy(xpath = "//div[1]/div[2]/div/div[2]/input")
	WebElement IPSlossAmountInput;
	
	@FindBy(xpath = "//div[2]/div[2]/div/div[2]/input")
	WebElement IPTProfitAmountInput;

	@FindBy(xpath = "//div[1]/div[3]/div/div[2]/input")
	WebElement IPSlossPIPsInput;
	
	@FindBy(xpath = "//div[2]/div[3]/div/div[2]/input")
	WebElement IPTprofitPIPsInput;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-6']")
	WebElement amountlossCheckbox;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-9']")
	WebElement amountProfitCheckBox;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-7']")
	WebElement pipsLossCheckBox;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-8']")
	WebElement pipsProfitCheckBox;
	
	@FindBy(xpath = "//div[3]/fieldset/div/div[1]//input")
	WebElement marginPercentInput;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-5']")
	WebElement positionCheckBoxME;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-7']")
	WebElement openPositionsSameDirectionCheckBox;
	
	@FindBy(xpath = ".//*[@id='gwt-uid-6']")
	WebElement cancelPendingOrdersCheckBox;
	
	public MoneyManagementRuleCreator(DriverProvider provider) {
		super(provider);
	}

	protected MoneyManagementRuleCreator(Builder builder) {
		super(builder.provider);
		String xpath = null;
		WebElement element;
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		Actions action = new Actions(getDriver());

		wait.until(ExpectedConditions.elementToBeClickable(addRuleButton));
		addRuleButton.click();

		nameOfRule.sendKeys(builder.rule.name);

		switch (builder.rule.type) {
		case "Trade Size":
			tradeSizeButton.click();

			tradeOptionLongList.click();
			if (builder.rule.longPosOption.contains("Fixed Amount"))
				fixedAmountLongPositions.click();
			else
				variableAmountLongPositions.click();
			

			tradeOptionShortList.click();

			if (builder.rule.shortPosOption.contains("Fixed Amount"))
				fixedAmountShortPositions.click();
			else
				variableAmountShortPositions.click();

			
			longPositionsAmountInput.clear();
			longPositionsAmountInput.sendKeys(builder.rule.longPosAmount);

			shortPositionsAmountInput.clear();
			shortPositionsAmountInput.sendKeys(builder.rule.shortPosAmount);

			break;
		case "Account Stop Loss":
			accountStopLossButton.click();

			drawdownPercentInput.clear();
			drawdownPercentInput.sendKeys(builder.rule.drawdownPercent);
			if (builder.rule.drawdownCB)
				drawdownCheckBox.click();

			equityDrawdownInput.clear();
			equityDrawdownInput.sendKeys(builder.rule.drawdownEquity);
			if (builder.rule.equityDrawDownCB)
				equityDrawdownCheckBox.click();

			positionList.click();
			if (builder.rule.closePos.contains("open"))
				closeOpenPositionsField.click();
			else if (builder.rule.closePos.contains("profitable"))
				closeProfitablePositionsField.click();
			else if (builder.rule.closePos.contains("losing"))
				closeLosingPositionsField.click();

			if (builder.rule.positionCB)
				positionCheckBox.click();
			if (builder.rule.pendingOrders)
				pendingOrdersCheckBox.click();
			if (builder.rule.deactivateTradingStrategies)
				deactivateTradingStrategiesCheckBox.click();

			break;
		case "Position Stop Loss":
			positionStopLossButton.click();

			IPSlossAmountInput.clear();
			IPSlossAmountInput.sendKeys(builder.rule.IPSlossAmount);
			
			IPTProfitAmountInput.clear();
			IPTProfitAmountInput.sendKeys(builder.rule.IPTprofitAmount);

			IPSlossPIPsInput.clear();
			IPSlossPIPsInput.sendKeys(builder.rule.IPSlossPIPs);

			IPTprofitPIPsInput.clear();
			IPTprofitPIPsInput.sendKeys(builder.rule.IPTprofitPIPs);

			if (builder.rule.amountCBloss)
				amountlossCheckbox.click();	
			if (builder.rule.amountCBprofit)
				amountProfitCheckBox.click();
			if (builder.rule.pipsCBloss)
				pipsLossCheckBox.click();
			if (builder.rule.pipsCBprofit)
				pipsProfitCheckBox.click();

			break;
		case "Maximum Exposure":
			maximumExposureButton.click();

			marginPercentInput.clear();
			marginPercentInput.sendKeys(builder.rule.marginPercent);

			positionList.click();
			if (builder.rule.closePos.contains("open"))
				closeOpenPositionsField.click();
			else if (builder.rule.closePos.contains("profitable"))
				closeProfitablePositionsField.click();
			else if (builder.rule.closePos.contains("losing"))
				closeLosingPositionsField.click();

			if (builder.rule.positionCB)
				positionCheckBoxME.click();
			if (builder.rule.openPosSameDir)
				openPositionsSameDirectionCheckBox.click();
			if (builder.rule.pendingOrders)
				cancelPendingOrdersCheckBox.click();

			break;
		}

	}

	public void saveMoneyManagementRule() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);

		saveRuleButton.click();
		wait.until(ExpectedConditions.visibilityOf(confirmMessage));

	}

	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public MoneyManagementRuleDescriptor rule = new MoneyManagementRuleDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T name(String value) {
			rule.name = value;
			return (T) this;
		}

		public T type(String value) {
			rule.type = value;
			return (T) this;
		}

		public T closePos(String value) {
			rule.closePos = value;
			return (T) this;
		}

		public T deactivateTradingStrategies(String value) {
			rule.deactivateTradingStrategies = Boolean.parseBoolean(value);
			return (T) this;
		}

		public T openPosSameDir(String value) {
			rule.openPosSameDir = Boolean.parseBoolean(value);
			return (T) this;
		}

		public T pendingOrders(String value) {
			rule.pendingOrders = Boolean.parseBoolean(value);
			return (T) this;
		}

		public T drawdownEquity(String value) {
			rule.drawdownEquity = value;
			return (T) this;
		}

		public T drawdownPercent(String value) {
			rule.drawdownPercent = value;
			return (T) this;
		}

		public T IPSlossAmount(String value) {
			rule.IPSlossAmount = value;
			return (T) this;
		}

		public T IPSlossPIPs(String value) {
			rule.IPSlossPIPs = value;
			return (T) this;
		}

		public T IPTprofitAmount(String value) {
			rule.IPTprofitAmount = value;
			return (T) this;
		}

		public T IPTprofitPIPs(String value) {
			rule.IPTprofitPIPs = value;
			return (T) this;
		}

		public T longPosAmount(String value) {
			rule.longPosAmount = value;
			return (T) this;
		}

		public T longPosOption(String value) {
			rule.longPosOption = value;
			return (T) this;
		}

		public T marginPercent(String value) {
			rule.marginPercent = value;
			return (T) this;
		}

		public T shortPosAmount(String value) {
			rule.shortPosAmount = value;
			return (T) this;
		}

		public T shortPosOption(String value) {
			rule.shortPosOption = value;
			return (T) this;
		}

		public T positionCB(String value) {
			rule.positionCB = Boolean.parseBoolean(value);
			return (T) this;
		}

		public T drawdownCB(String value) {
			rule.drawdownCB = Boolean.parseBoolean(value);
			return (T) this;
		}

		public T equityDrawDownCB(String value) {
			rule.equityDrawDownCB = Boolean.parseBoolean(value);
			return (T) this;
		}
		
		public T amountCBloss(String value) {
			rule.amountCBloss = Boolean.parseBoolean(value);
			return (T) this;
		}

		public T amountCBprofit(String value) {
			rule.amountCBprofit = Boolean.parseBoolean(value);
			return (T) this;
		}

		public T pipsCBloss(String value) {
			rule.pipsCBloss = Boolean.parseBoolean(value);
			return (T) this;
		}
		
		public T pipsCBprofit(String value) {
			rule.pipsCBprofit = Boolean.parseBoolean(value);
			return (T) this;
		}
		
		public MoneyManagementRuleCreator build() {
			return new MoneyManagementRuleCreator(this);
		}

	}

}
