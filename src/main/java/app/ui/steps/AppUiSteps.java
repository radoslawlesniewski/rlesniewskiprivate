package app.ui.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.fest.assertions.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.Feedback;
import app.ui.pages.MailinatorPage;
import app.ui.pages.AccountPage;
import app.ui.pages.MagickPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator;
import app.ui.pages.indicators.BaseIndicatorsCreator;
import app.ui.pages.indicators.IndicatorCreator;
import app.ui.pages.indicators.IndicatorCreatorSpecificType;
import app.ui.pages.indicators.IndicatorsDescriptor;
import app.ui.pages.rules.MoneyManagementRuleCreator;
import app.ui.pages.rules.SignalCreator;
import app.ui.pages.rules.TradeTimingRuleCreator;
import app.ui.tradingAccount.TradingAccountCreator;
import app.ui.webdriver.DriverProvider;

public class AppUiSteps extends BasePage {
	public MagickPage loginPage;
	public AccountPage accountPage;
	public BaseIndicatorsCreator mainIndicator;
	public IndicatorCreator indicatorCreator;
	public AlligatorIndicatorCreator alligatorIndicatorCreator;
	public IndicatorCreatorSpecificType IndicatorCreatorSpecificType;
	public SignalCreator signalCreator;
	public TradeTimingRuleCreator tradeTimingRuleCreator;
	public static int iterationSignalCreator;
	public MoneyManagementRuleCreator moneyManagementRuleCreator;
	public Feedback feedback;
	public MailinatorPage mailinatorPage;
	public TradingAccountCreator account;
	

	public AppUiSteps(DriverProvider provider) {
		super(provider);
	}

	@Given("I am on the Magick login page")
	public void openHomePage() throws InterruptedException {
		loginPage = new MagickPage(getProvider());
		loginPage.init();
	}

	@Given("I previosly logged in to account using '<username>' and '<password>' and validated that I landed on account page")
	@Alias("I previosly logged in to account using '$username' and '$password' and validated that I landed on account page")
	@Composite(steps = {
			"Given I am on the Magick login page",
			"When I am loging in using following '<username>' username and '<password>' password",
			"Then I am landing on my account page" })
	public void logInAndValidate(@Named("username") String username,
			@Named("password") String password) {

	}

	@Given("I previosly logged in to account using '<username>' and '<password>' and created new Strategy called '<name>'")
	@Composite(steps = {
			"Given I previosly logged in to account using '<username>' and '<password>' and validated that I landed on account page",
			"When I am creating new strategy called '<name>'",
			"Then I am landing on my account page" })
	public void login(@Named("username") String username,
			@Named("password") String password, @Named("name") String name) {
	}

	@When("I am loging in using following '$username' username and '$password' password")
	@Alias("I am loging in using following '<username>' username and '<password>' password")
	public void logInToEmail(@Named("username") String username,
			@Named("password") String password) {
		loginPage = new MagickPage(getProvider());
		loginPage.logIn(username, password);
	}

	@When("I am creating new strategy called '<nameOfStrategy>'")
	@Alias("I am creating new strategy called '$nameOfStrategy'")
	public void createNewStrategy(@Named("nameOfStrategy") String name) {
		accountPage = new AccountPage(getProvider());
		accountPage.createNewStrategy(name);
	}
	
	@When("I am sending a message (in tab: feedback)")
	public void createNewMessage(@Named("question") String question, @Named("details") String details, @Named("name") String name, @Named("emailAddress") String emailAddress){
		feedback = new Feedback.Builder(
				getProvider())
				.question(question)
				.details(details)
				.name(name)
				.emailAddress(emailAddress)
				.build();
		feedback = new Feedback(getProvider());
	}
	
	@When("I am creating new Trading Account")
	public void createTradingAccount(@Named("description") String description, @Named("broker") String broker, @Named("server") String server, @Named("usernameAccount") String username,  @Named("passwordAccount") String password){
		account = new TradingAccountCreator.Builder(
				getProvider())
				.description(description)
				.broker(broker)
				.server(server)
				.username(username)
				.password(password)
				.build();
		account = new TradingAccountCreator(getProvider());
		
	}
	
	@When("I am log in and out n-times using following '<username>' username and '<password>' password")
	public void LogInAndOut(@Named("username") String username,@Named("password") String password) {
		logger.info("!!!!!!!!!!   1");
		String env = System.getProperty("iterations");
		logger.info("!!!!!!!!!!   2");
		if (env==null) {
			env = "1";
		}
		int a = Integer.parseInt(env);
		for (int i=0; i<a; i++) {
		loginPage = new MagickPage(getProvider());
		loginPage.logIn(username, password);
		accountPage = new AccountPage(getProvider());
		accountPage.validatePage();
		//accountPage.logOut();
		}
	}
	

	@Then("I am adding Indicator '<typeOfIndicator>'")
	public void addSpecificIndicator(
			@Named("typeOfIndicator") String typeOfIndicator,
			@Named("symbol") String symbol,
			@Named("timeFrame") String timeFrame,
			@Named("jawsPeriod") String jawsPeriod,
			@Named("nameOfIndicator") String name,
			@Named("teethPeriod") String teethPeriod,
			@Named("lipsPeriod") String lipsPeriod,
			@Named("jawsShift") String jawsShift,
			@Named("teethShift") String teethShift,
			@Named("lipsShift") String lipsShift,
			@Named("applyTo") String applyTo,
			@Named("maMethod") String maMethod, @Named("period") String period,
			@Named("shift") String shift,
			@Named("deviations") String deviations,
			@Named("deviation") String deviation,
			@Named("tenkanSen") String tenkanSen,
			@Named("kijunSen") String kijunSen,
			@Named("senkouSpanB") String senkouSpanB,
			@Named("fastEMA") String fastEMA, @Named("slowEMA") String slowEMA,
			@Named("macdSMA") String macdSMA, @Named("step") String step,
			@Named("maximum") String maximum, @Named("dPeriod") String dPeriod,
			@Named("kPeriod") String kPeriod, @Named("slowing") String slowing,
			@Named("priceField") String priceField) {
		IndicatorCreatorSpecificType = new IndicatorCreatorSpecificType.Builder(
				getProvider()).applyTo(applyTo).deviation(deviation)
				.deviations(deviations).dPeriod(dPeriod).fastEMA(fastEMA)
				.jawsPeriod(jawsPeriod).jawsShift(jawsShift).kijunSen(kijunSen)
				.kPeriod(kPeriod).lipsPeriod(lipsPeriod).lipsShift(lipsShift)
				.macdSMA(macdSMA).maMethod(maMethod).maximum(maximum)
				.name(name).period(period).priceField(priceField)
				.senkouSpanB(senkouSpanB).shift(shift).slowEMA(slowEMA)
				.slowing(slowing).step(step).symbol(symbol)
				.teethPeriod(teethPeriod).teethShift(teethShift)
				.tenkanSen(tenkanSen).timeFrame(timeFrame)
				.typeOfIndicator(typeOfIndicator).build();
		IndicatorCreatorSpecificType = new IndicatorCreatorSpecificType(
				getProvider());

	}
	
	@Then("I am loging out")
	public void logOut() {
		accountPage = new AccountPage(getProvider());
		//accountPage.logOut();
	}
	
	@Then("I am doing nothing")
	public void doNothing() {
		
	}


	@Then("I am landing on my account page")
	public void validatePage() {
		accountPage = new AccountPage(getProvider());
		accountPage.validatePage();
	}

	@Then("I am adding '$market' market")
	@Alias("I am adding '<market>' market")
	public void addMarket(@Named("market") String market) {
		accountPage = new AccountPage(getProvider());
		accountPage.addMarket(market);
	}
	
	
	@Then("I am going to first strategy and removing all test indicators")
	public void enterStrategyAndRemoveInd(){
		accountPage = new AccountPage(getProvider());
		//accountPage.goToFirstStrategy();
		indicatorCreator = new IndicatorCreator(getProvider());
		indicatorCreator.removeAllIndicators();
	}
	


	@Then("I am adding every Indicator of <type> type")
	public void addEveryIndicator(@Named("type") String typeOfIndicator) {
		mainIndicator = new BaseIndicatorsCreator(getProvider());
		mainIndicator.addEveryIndicatorOfType(typeOfIndicator);
	}

	@Then("I am deleting this Indicator and checking if operation was succesful")
	public void DeletingIndicatorAndCheckingIfSuccessful(
			@Named("nameOfIndicator") String nameOfIndicator) {
		/*indicatorCreator.deleteIndicator(nameOfIndicator
				+ AccountPage.SystemCurrentTimeMillis);
		assertTrue("True", accountPage.checkIfMessageIsShown());
*/
	}

	@Then("I am checking if message with error is visible")
	public void checkIfMessageIsVisible() {

		assertTrue("True", accountPage.checkIfMessageIsShown());
	}

	@Then("I am adding a signal with: $ranksTable")
	public void signalCreator(ExamplesTable ranksTable) {
		
		iterationSignalCreator=0;
		for (Map<String, String> row : ranksTable.getRows()) {
			signalCreator = new SignalCreator.Builder(getProvider())
					.name(row.get("nameOfSignal")).type(row.get("type"))
					.symbol(row.get("symbol"))
					.symbolOption(row.get("symbolOption"))
					.nameOfIndicator(row.get("indicator"))
					.indicatorField(row.get("indicatorField"))
					.logicOperator(row.get("logic"))
					.amount(row.get("amount")).join(row.get("join")).build();
			signalCreator = new SignalCreator(getProvider());

		}
		signalCreator.saveSignal();

	}
	

	@Then("I am adding new trade timing rule with<name> (optionally with signal)")
	public void ruleCreator(@Named("name") String name, @Named("type") String type, @Named("sameDirection") String sameDirection, @Named("oppositeDirection") String oppositeDirection, @Named("bearBullSignal") String bearBullSignal, @Named("ruleType") String ruleType, @Named("symbol") String symbol, @Named("symbolOption") String symbolOption, @Named("indicator") String indicator, @Named("indicatorField") String indicatorField, @Named("logic") String logic, @Named("amount") String amount, @Named("customRule") String customRule){

		tradeTimingRuleCreator = new TradeTimingRuleCreator.Builder(getProvider())
								.name(name)
								.type(type)
								.sameDirection(sameDirection)
								.oppositeDirection(oppositeDirection)
								.bearBullSignal(bearBullSignal)
								.ruleType(ruleType)
								.amount(amount)
								.indicatorField(indicatorField)
								.nameOfIndicator(indicator)
								.logicOperator(logic)
								.symbol(symbol)
								.symbolOption(symbolOption)
								.customRule(customRule)
								.build();
		tradeTimingRuleCreator= new TradeTimingRuleCreator(getProvider());
		

	tradeTimingRuleCreator.saveRule();
	}
	
	@Then("I am adding new money management rule with <name>")
	public void moneyRuleCreator(@Named("name") String name, @Named("type") String type, @Named("longPosOption") String longPosOption, @Named("shortPosOption") String shortPosOption, @Named("longPosAmount") String longPosAmount, @Named("shortPosAmount") String shortPosAmount, @Named("drawdownPercent") String drawdownPercent, @Named("drawdownEquity") String drawdownEquity, @Named("closePos") String closePos, @Named("pendingOrders") String pendingOrders, @Named("deactivateTradingStrategies") String deactivateTradingStrategies, @Named("IPSlossAmount") String IPSlossAmount, @Named("IPSlossPIPs") String IPSlossPIPs, @Named("IPTprofitAmount") String IPTprofitAmount, @Named("IPTprofitPIPs") String IPTprofitPIPs, @Named("marginPercent") String marginPercent, @Named("openPosSameDir") String openPosSameDir, @Named("positionCB") String positionCB, @Named("drawdownCB") String drawdownCB, @Named("equityDrawDownCB") String equityDrawDownCB, @Named("amountCBloss") String amountCBloss, @Named("amountCBprofit") String amountCBprofit,  @Named("pipsCBloss") String pipsCBloss,  @Named("pipsCBprofit") String pipsCBprofit){

		moneyManagementRuleCreator = new MoneyManagementRuleCreator.Builder(getProvider())
								.name(name)
								.type(type)
								.closePos(closePos)
								.deactivateTradingStrategies(deactivateTradingStrategies)
								.drawdownEquity(drawdownEquity)
								.drawdownPercent(drawdownPercent)
								.IPSlossAmount(IPSlossAmount)
								.IPSlossPIPs(IPSlossPIPs)
								.IPTprofitAmount(IPTprofitAmount)
								.IPTprofitPIPs(IPTprofitPIPs)
								.longPosAmount(longPosAmount)
								.longPosOption(longPosOption)
								.marginPercent(marginPercent)
								.openPosSameDir(openPosSameDir)
								.pendingOrders(pendingOrders)
								.shortPosAmount(shortPosAmount)
								.shortPosOption(shortPosOption)
								.positionCB(positionCB)
								.drawdownCB(drawdownCB)
								.equityDrawDownCB(equityDrawDownCB)
								.amountCBloss(amountCBloss)
								.amountCBprofit(amountCBprofit)
								.pipsCBloss(pipsCBloss)
								.pipsCBprofit(pipsCBprofit)
								.build();
		moneyManagementRuleCreator = new MoneyManagementRuleCreator(getProvider());
		
		moneyManagementRuleCreator.saveMoneyManagementRule();
	}
	
	@Then("I am logging in to mailinator and checking for reply")
	public void checkMailinator(@Named("user") String user,@Named("question") String question){
		mailinatorPage = new MailinatorPage(getProvider());
		mailinatorPage.init();
		mailinatorPage.typeUserLogin(user);
		assertTrue(mailinatorPage.subjectOfMessage(question));
		
	}
	
	@Then("I am looking for account and deleting it")
	public void deleteAccount(@Named("description") String description){
		
		account.searchOnListAndDeleteAccount(description);
		
	}
	
	@Then("I am validating error message")
	public void validateErrorMessage(){
		assertThat(accountPage.checkIfMessageIsShown()).isTrue();
		assertThat(accountPage.checkMessageProperty("exception")).isFalse();
		assertThat(accountPage.checkMessageProperty("Rule Saved Successfully!")).isFalse();
	}


}