package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

public class EnvelopesIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[3]//div[3]/select")
	WebElement maMethod;

	@FindBy(xpath = "//tr[4]//tr[4]//div[3]/select")
	WebElement applyTo;
	
	@FindBy(xpath = "//tr[1]//div[3]/input")
	WebElement period;
	
	@FindBy(xpath = "//tr[2]//div[3]/input")
	WebElement shift;
	
	@FindBy(xpath = "//tr[5]//div[3]/input")
	WebElement deviation;
	
			
	public EnvelopesIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	
	private EnvelopesIndicatorCreator(Builder builder) {
		super(builder);
		
		String xpath;
		WebElement typeOneFromTheList;
		Actions action = new Actions(getDriver());
		
		if(!builder.indicatorDescriptor.maMethod.isEmpty()){
		maMethod.click();
		
		xpath = "//tr[3]//div[3]/select/*[.='"+ builder.indicatorDescriptor.maMethod +"']";
		typeOneFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
		}
		
		if(!builder.indicatorDescriptor.applyTo.isEmpty()){
		applyTo.click();

		xpath = "//tr[4]//tr[4]//div[3]/select/*[.='"+ builder.indicatorDescriptor.applyTo +"']";
		typeOneFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
		}
		if(!builder.indicatorDescriptor.period.isEmpty()){
		period.clear();
		period.sendKeys(builder.indicatorDescriptor.period);}
		if(!builder.indicatorDescriptor.shift.isEmpty()){
		shift.clear();
		shift.sendKeys(builder.indicatorDescriptor.shift);}
		if(!builder.indicatorDescriptor.deviation.isEmpty()){
		deviation.clear();
		deviation.sendKeys(builder.indicatorDescriptor.deviation);}
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		public Builder maMethod(String value) {
			indicatorDescriptor.maMethod = value;
			return this;
		}
		
		public Builder applyTo(String value) {
			indicatorDescriptor.applyTo = value;
			return this;
		}
		
		public Builder period(String value) {
			indicatorDescriptor.period = value;
			return this;
		}
		
		public Builder shift(String value) {
			indicatorDescriptor.shift = value;
			return this;
		}
		
		public Builder deviation(String value) {
			indicatorDescriptor.deviation = value;
			return this;
		}

		
		public EnvelopesIndicatorCreator build(){
			return new EnvelopesIndicatorCreator(this);
		}
	}

}