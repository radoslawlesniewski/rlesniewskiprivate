package app.ui.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import app.ui.common.BasePage;
import app.ui.webdriver.DriverProvider;

public class MagickPage extends BasePage implements Page {

	@FindBy(css="#username")
	public WebElement username;
	
	@FindBy(css="#password")
	public WebElement password;
	
	@FindBy(css=".btn.btn-primary")
	public WebElement loginButton;
	
	public MagickPage(DriverProvider provider) {
		super(provider);
		PageFactory.initElements(getDriver(), this);
	}
	
	@Override
	public void init() {
		getDriver().manage().deleteAllCookies();
		getDriver().get("http://beta.magick.nu");
		getDriver().manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	}

	
	
	public void logIn(String user, String pass) {
		username.sendKeys(user);
		password.sendKeys(pass);
		loginButton.click();
	}

}
