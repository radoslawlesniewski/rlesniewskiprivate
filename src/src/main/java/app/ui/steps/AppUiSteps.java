package app.ui.steps;

import static org.hamcrest.MatcherAssert.assertThat;

import org.hamcrest.Matchers;
import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.annotations.Composite;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.MagickPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator;
import app.ui.pages.indicators.BaseIndicatorsCreator;
import app.ui.pages.indicators.IndicatorCreator;
import app.ui.pages.indicators.IndicatorsDescriptor;
import app.ui.webdriver.DriverProvider;

public class AppUiSteps extends BasePage {
	public MagickPage loginPage;
	public AccountPage accountPage;
	public BaseIndicatorsCreator mainIndicator;
	public IndicatorCreator indicatorCreator;
	public AlligatorIndicatorCreator alligatorIndicatorCreator;
	
	public AppUiSteps(DriverProvider provider) {
		super(provider);
	}

	@Given("I am on the Magick login page")
	public void openHomePage() throws InterruptedException {
		loginPage = new MagickPage(getProvider());
		loginPage.init();
	}
	
	@Given("I previosly logged in to account using '<username>' and '<password>' and validated that I landed on account page")
	@Composite(steps = {
			"Given I am on the Magick login page",
			"When I am loging in using following '<username>' username and '<password>' password",
			"Then I am landing on my account page"	
	})
	public void logInAndValidate(@Named("username") String username,@Named("password") String password) {
		
	}
		
	@When("I am loging in using following '$username' username and '$password' password")
	@Alias("I am loging in using following '<username>' username and '<password>' password")
	public void logInToEmail(@Named("username") String username,@Named("password") String password){
		loginPage = new MagickPage(getProvider());
		loginPage.logIn(username, password);
	}
	
	@When("I am creating new strategy called '$name'")
	@Alias("I am creating new strategy called '<name>'")
	public void createNewStrategy(@Named("name") String name) {
		accountPage = new AccountPage(getProvider());
		accountPage.createNewStrategy(name);
	}
	
	@Then("I am landing on my account page")
	public void validatePage() {
		accountPage = new AccountPage(getProvider());
		accountPage.validatePage();
	}
	
	@Then("I am adding '$market' market")
	@Alias("I am adding '<market>' market")
	public void addMarket(@Named("market")String market){
		accountPage = new AccountPage(getProvider());
		accountPage.addMarket(market);
	}
	
	@Then("I am adding Indicator with following '<type>' type, '<symbol>' symbol and '<timeFrame>' time frame (also optionally: '<jawsPeriod>' jawsPeriod)")
	public void addIndicator(@Named("type") String typeOfIndicator,@Named("symbol") String symbol, @Named("timeFrame") String timeFrame, @Named("jawsPeriod") String jawsPeriod) {
		alligatorIndicatorCreator = (AlligatorIndicatorCreator) new AlligatorIndicatorCreator
			.Builder(getProvider())
			.typeOfIndicator(typeOfIndicator)
			.symbol(symbol)
			.timeFrame(timeFrame)
			.jawsPeriod(jawsPeriod)
			.build();
		alligatorIndicatorCreator = new AlligatorIndicatorCreator(getProvider());
		alligatorIndicatorCreator.saveIndicator();
	}
	
	@Then("I am adding every Indicator of <type> type")
	public void addEveryIndicator(@Named("type") String typeOfIndicator) {
		mainIndicator = new BaseIndicatorsCreator(getProvider());
		mainIndicator.addEveryIndicatorOfType(typeOfIndicator);
	}
	
}
