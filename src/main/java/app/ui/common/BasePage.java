package app.ui.common;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.ui.webdriver.DriverProvider;

public class BasePage {
	private DriverProvider driverProvider;
	protected static final int DRIVER_WAIT = 30;
	protected static Logger logger = LoggerFactory.getLogger(BasePage.class);

	public BasePage(DriverProvider provider) {
		this.driverProvider = provider;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(
				getDriver(), DRIVER_WAIT);
		PageFactory.initElements(finder, this);
	}

	public void switchTo(String windowTitle) {
		WebDriver driver = getDriver();
		Set<String> windows = driver.getWindowHandles();

		for (String window : windows) {
			driver.switchTo().window(window);
			if (driver.getTitle().contains(windowTitle)) {
				return;
			}
		}
	}

	public void handleMultipleWindows(String windowTitle) {
		Set<String> windows = getDriver().getWindowHandles();

		for (String window : windows) {
			getDriver().switchTo().window(window);
			if (getDriver().getTitle().contains(windowTitle)) {
				return;
			}
		}
	}

	protected DriverProvider getProvider() {
		return driverProvider;
	}

	protected WebDriver getDriver() {
		return driverProvider.get();
	}

}
