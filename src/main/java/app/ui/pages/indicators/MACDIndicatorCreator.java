package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.AlligatorIndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

public class MACDIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[4]//div[3]/select")
	WebElement applyTo;
	
	@FindBy(xpath = "//tr[1]//div[3]/input")
	WebElement fastEMA;
	
	@FindBy(xpath = "//tr[2]//div[3]/input")
	WebElement slowEMA;
	
	@FindBy(xpath = "//tr[4]//tr[3]//div[3]/input")
	WebElement macdSMA;
	
	public MACDIndicatorCreator(DriverProvider provider) {
		super(provider);
	}
	

	private MACDIndicatorCreator(Builder builder) {
		super(builder);
		
		applyTo.click();

		String xpath = "//tr[4]//div[3]/select/*[.='"+ builder.indicatorDescriptor.applyTo +"']";
		WebElement typeFromTheList = getDriver().findElement(By.xpath(xpath));

		Actions action = new Actions(getDriver());
		action.moveToElement(typeFromTheList).clickAndHold(typeFromTheList)
		.release().build().perform();
		fastEMA.clear();
		fastEMA.sendKeys(builder.indicatorDescriptor.fastEMA);
		slowEMA.clear();
		slowEMA.sendKeys(builder.indicatorDescriptor.slowEMA);
		macdSMA.clear();
		macdSMA.sendKeys(builder.indicatorDescriptor.macdSMA);
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		
		public Builder applyTo(String value) {
			indicatorDescriptor.applyTo = value;
			return this;
		}
		
		public Builder fastEMA(String value) {
			indicatorDescriptor.fastEMA = value;
			return this;
		}
		
		public Builder slowEMA(String value) {
			indicatorDescriptor.slowEMA = value;
			return this;
		}
		
		public Builder macdSMA(String value) {
			indicatorDescriptor.macdSMA = value;
			return this;
		}
		
		
		public MACDIndicatorCreator build(){
			return new MACDIndicatorCreator(this);
		}
	}

}