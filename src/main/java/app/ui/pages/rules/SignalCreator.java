package app.ui.pages.rules;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.IndicatorCreator;
import app.ui.pages.indicators.IndicatorsDescriptor;
import app.ui.pages.indicators.IndicatorCreator.Builder;
import app.ui.steps.AppUiSteps;
import app.ui.webdriver.DriverProvider;

public class SignalCreator extends BasePage {

	@FindBy(xpath = "//div[3]/div[2]//h2/a")
	WebElement addSignalButton;

	@FindBy(css = ".input-xlarge.focused")
	WebElement nameOfSignal;

	//@FindBy(css = ".switch-right.switch-danger")
	@FindBy(xpath = ".//*[@id='toggle-state-switch']/div")
	WebElement switchType;

	@FindBy(css = "#indicatorBtn")
	WebElement indicatorButton;

	@FindBy(css = "#symbolBtn")
	WebElement symbolButton;

	@FindBy(css = "#amountBtn")
	WebElement amountButton;

	@FindBy(css = "#logicBtn")
	WebElement logicButton;

	@FindBy(css = "#joinBtn")
	WebElement joinButton;

	@FindBy(css = ".btn.btn-primary.save-btn")
	WebElement saveSignalButton;

	@FindBy(css = ".ui-pnotify-text")
	WebElement confirmMessage;

	@FindBy(xpath = "//span[.='..Group..']/..")
	WebElement groupList;
	
	public SignalCreator(DriverProvider provider) {
		super(provider);
	}

	protected SignalCreator(Builder builder) {
		super(builder.provider);

		Actions action = new Actions(getDriver());
		String xpath;
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);

		if (AppUiSteps.iterationSignalCreator == 0) {
			wait.until(ExpectedConditions.elementToBeClickable(addSignalButton));
			addSignalButton.click();

			nameOfSignal.sendKeys(builder.signal.name);

			if (!builder.signal.type.contains("Bear Signal"))
				//getDriver().findElement(By.xpath(switchType)).click();
				switchType.click();
		}

		if(!builder.signal.symbol.isEmpty()){
			
			Symbol symbol = new Symbol.Builder(getProvider())
			.symbol(builder.signal.symbol)
			.symbolOption(builder.signal.symbolOption)
			.logicOperator(builder.signal.logicOperator)
			.amount(builder.signal.amount).build();
			symbol = new Symbol(getProvider());
	
		}
		
		if(!builder.signal.nameOfIndicator.isEmpty()){
			
			Indicator indicator = new Indicator.Builder(getProvider())
			.nameOfIndicator(builder.signal.nameOfIndicator)
			.indicatorField(builder.signal.indicatorField)
			.logicOperator(builder.signal.logicOperator)
			.amount(builder.signal.amount).build();
			indicator = new Indicator(getProvider());
			
		}

		if (!builder.signal.join.isEmpty()) {
			wait.until(ExpectedConditions.elementToBeClickable(joinButton));
			joinButton.click();
			groupList.click();

			xpath = ".//*[@id='chozen_container__3_chzn']//*[@class='GOL5FQVBPI']/li[.='"
					+ builder.signal.join + "']";
			WebElement groupFromTheList = getDriver().findElement(By.xpath(xpath));

			action.moveToElement(groupFromTheList).clickAndHold(groupFromTheList)
					.release().build().perform();
			AppUiSteps.iterationSignalCreator++;

		}

	}

	public void saveSignal() {
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);

		saveSignalButton.click();
		wait.until(ExpectedConditions.visibilityOf(confirmMessage));

	}

	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public RuleDescriptor signal = new RuleDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T name(String value) {
			signal.name = value;
			return (T) this;
		}

		public T type(String value) {
			signal.type = value;
			return (T) this;
		}

		public T nameOfIndicator(String value) {
			signal.nameOfIndicator = value;
			return (T) this;
		}

		public T indicatorField(String value) {
			signal.indicatorField = value;
			return (T) this;
		}

		public T logicOperator(String value) {
			signal.logicOperator = value;
			return (T) this;
		}

		public T symbol(String value) {
			signal.symbol = value;
			return (T) this;
		}

		public T symbolOption(String value) {
			signal.symbolOption = value;
			return (T) this;
		}

		public T join(String value) {
			signal.join = value;
			return (T) this;
		}

		public T amount(String value) {
			signal.amount = value;
			return (T) this;
		}

		public SignalCreator build() {
			return new SignalCreator(this);
		}

	}
}
