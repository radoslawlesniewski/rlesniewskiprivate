package app.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.ui.common.BasePage;
import app.ui.pages.AccountPage;
import app.ui.pages.indicators.IndicatorCreator;
import app.ui.pages.indicators.IndicatorsDescriptor;
import app.ui.pages.indicators.IndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;
import app.ui.webdriver.listeners.MyEventListener;

public class Feedback extends BasePage {

	@FindBy(xpath = ".//*[@id='feedback']/a")
	WebElement feedbackButton;
	
	@FindBy(css = "#subject")
	WebElement questionField;

	@FindBy(css = "#description")
	WebElement detailsField;
	
	@FindBy(css = "#name")
	WebElement nameField;
	
	@FindBy(css = "#email")
	WebElement emailField;
	
	@FindBy(css = "#dropbox_submit")
	WebElement submitButton;
	
	public Feedback(DriverProvider provider) {
		super(provider);
	}

	protected Feedback(Builder builder) {
		super(builder.provider);

		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(feedbackButton));
		
		feedbackButton.click();
		getDriver().switchTo().frame(1);
		
		MyEventListener.addIgnoredElement("By.selector: #subject");
		MyEventListener.addIgnoredElement("By.xpath: .//*[@id='subject']");
		MyEventListener.addIgnoredElement("By.selector: #description");
		MyEventListener.addIgnoredElement("By.selector: #name");
		MyEventListener.addIgnoredElement("By.selector: #email");
	

		questionField.sendKeys(builder.feedback.question + System.currentTimeMillis());
		detailsField.sendKeys(builder.feedback.details);
		nameField.sendKeys(builder.feedback.name);
		emailField.sendKeys(builder.feedback.emailAddress);
		
		submitButton.click();
		
	}

	public static class Builder<T extends Builder> {
		DriverProvider provider;
		WebDriver driver;
		public FeedbackDescriptor feedback = new FeedbackDescriptor();

		public Builder(DriverProvider value) {
			provider = value;
			driver = provider.get();
		}

		public T question(String value) {
			feedback.question = value;
			return (T) this;
		}

		public T details(String value) {
			feedback.details = value;
			return (T) this;
		}

		public T name(String value) {
			feedback.name = value;
			return (T) this;
		}

		public T emailAddress(String value) {
			feedback.emailAddress = value;
			return (T) this;
		}

		public Feedback build() {
			return new Feedback(this);
		}

	}

}
