package app.ui.pages.indicators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import app.ui.pages.indicators.EnvelopesIndicatorCreator.Builder;
import app.ui.webdriver.DriverProvider;

public class EnvelopesSecondTypeIndicatorCreator extends IndicatorCreator {
	
	@FindBy(xpath = "//tr[3]//div[3]/select")
	WebElement maMethod;
	
	@FindBy(xpath = "//tr[2]//div[3]/select")
	WebElement applyTo;
	
	@FindBy(xpath = "//tr[1]//div[3]/input")
	WebElement period;
	

	public EnvelopesSecondTypeIndicatorCreator(DriverProvider provider) {
		super(provider);
		
	}
	
	private EnvelopesSecondTypeIndicatorCreator(Builder builder) {
		super(builder);
		
		String xpath;
		WebElement typeOneFromTheList;
		Actions action = new Actions(getDriver());
		
		if(!builder.indicatorDescriptor.maMethod.isEmpty()){
		maMethod.click();
		
		xpath = "//tr[3]//div[3]/select/*[.='"+ builder.indicatorDescriptor.maMethod +"']";
		typeOneFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
		}
		
		if(!builder.indicatorDescriptor.applyTo.isEmpty()){
		applyTo.click();

		xpath = "//tr[2]//div[3]/select/*[.='"+ builder.indicatorDescriptor.applyTo +"']";
		typeOneFromTheList = getDriver().findElement(By.xpath(xpath));
		action.moveToElement(typeOneFromTheList).clickAndHold(typeOneFromTheList)
		.release().build().perform();
		}
		if(!builder.indicatorDescriptor.period.isEmpty()){
		period.clear();
		period.sendKeys(builder.indicatorDescriptor.period);}
		
	}
	
	public static class Builder extends IndicatorCreator.Builder<Builder> {
		public static DriverProvider provider;
		IndicatorsDescriptor indicatorDescriptor = new IndicatorsDescriptor();
		
		public Builder(DriverProvider value) {
			super(provider = value);
		}
		public Builder maMethod(String value) {
			indicatorDescriptor.maMethod = value;
			return this;
		}
		
		public Builder applyTo(String value) {
			indicatorDescriptor.applyTo = value;
			return this;
		}
		
		public Builder period(String value) {
			indicatorDescriptor.period = value;
			return this;
		}
		
		
		public EnvelopesSecondTypeIndicatorCreator build(){
			return new EnvelopesSecondTypeIndicatorCreator(this);
		}
	}


}
